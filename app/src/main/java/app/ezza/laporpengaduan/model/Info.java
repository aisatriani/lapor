package app.ezza.laporpengaduan.model;

/**
 * Created by azisa on 6/28/2017.
 */

public class Info {

    private String senderUid;
    private String title;
    private String content;
    private long created_at;

    public Info() {
        this.created_at = System.currentTimeMillis();
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreated_at() {
        return created_at;
    }

}
