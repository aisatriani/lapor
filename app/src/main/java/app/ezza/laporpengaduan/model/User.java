package app.ezza.laporpengaduan.model;

import java.io.Serializable;

/**
 * Created by azisa on 6/26/2017.
 */

public class User implements Serializable {

    public static final int TYPE_MEMBER = 300;
    public static final int TYPE_PENDIDIKAN = 1;
    public static final int TYPE_KESEHATAN = 2;
    public static final int TYPE_PU = 3;
    public static final int TYPE_PEMUKIMAN = 4;
    public static final int TYPE_POLSEK = 5;
    public static final int TYPE_SOSIAL = 6;
    public static final int TYPE_BUPATI = 100;
    public static final int TYPE_ADMINISTRATOR = 200;

    private String uid;
    private String nama;
    private String username;
    private String email;
    private String password;
    private String telp;
    private String tokenFcm;
    private String alamat;
    private String image_url;
    private int type;
    private String noktp;
    private boolean ktpVerify;
    private boolean ktpCancel;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTokenFcm() {
        return tokenFcm;
    }

    public void setTokenFcm(String tokenFcm) {
        this.tokenFcm = tokenFcm;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }

    public String getNoktp() {
        return noktp;
    }

    public void setNoktp(String noktp) {
        this.noktp = noktp;
    }

    public boolean isKtpVerify() {
        return ktpVerify;
    }

    public void setKtpVerify(boolean ktpVerify) {
        this.ktpVerify = ktpVerify;
    }

    public boolean isKtpCancel() {
        return ktpCancel;
    }

    public void setKtpCancel(boolean ktpCancel) {
        this.ktpCancel = ktpCancel;
    }
}
