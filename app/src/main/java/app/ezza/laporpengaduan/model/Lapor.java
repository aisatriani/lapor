package app.ezza.laporpengaduan.model;

import java.io.Serializable;

/**
 * Created by azisa on 6/30/2017.
 */

public class Lapor implements Serializable {

    public static final String TOPIK_PENDIDIKAN = "pendidikan";
    public static final String TOPIK_KESEHATAN = "kesehatan";
    public static final String TOPIK_PEKERJAAN_UMUM = "pu";
    public static final String TOPIK_PEMUKIMAN = "pemukiman";
    public static final String TOPIK_POLSEK = "polsek";
    public static final String TOPIK_SOSIAL = "sosial";

    //status laporan masih dalam proses review
    public static final String STATUS_ON_PROCESS = "process";
    //status laporan telah di terima. dan menunggu untuk di proses
    public static final String STATUS_ON_ACCEPT = "accept";
    //status laporan berhasil di proses, dan akan di eksekusi segera
    public static final String STATUS_ON_SUCCESS = "success";
    //status laporan di batalkan
    public static final String STATUS_ON_CANCEL = "cancel";

    private String uid;
    private double latitude;
    private double longitude;
    private String topikSubscriber;
    private String imgUrl;
    private String desc;
    private String senderUid;
    private String status;
    private Eksekusi eksekusi;
    private String tolak;
    private long created_at;



    public Lapor() {
        this.created_at = System.currentTimeMillis();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTopikSubscriber() {
        return topikSubscriber;
    }

    public void setTopikSubscriber(String topikSubscriber) {
        this.topikSubscriber = topikSubscriber;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getCreated_at() {
        return created_at;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Eksekusi getEksekusi() {
        return eksekusi;
    }

    public void setEksekusi(Eksekusi eksekusi) {
        this.eksekusi = eksekusi;
    }

    public String getTolak() {
        return tolak;
    }

    public void setTolak(String tolak) {
        this.tolak = tolak;
    }
}
