package app.ezza.laporpengaduan.model;

import java.io.Serializable;

/**
 * Created by azisa on 8/15/2017.
 */

public class Eksekusi implements Serializable {
    public String imgurl;
    public String keterangan;

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

}
