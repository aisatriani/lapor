package app.ezza.laporpengaduan.model;

/**
 * Created by azisa on 7/3/2017.
 */

public class Topik {
    private static final String TAG = Topik.class.getSimpleName();
    public static final String ALL = "all";
    public static final String BUPATI = "bupati";
    public static final String KESEHATAN = "kesehatan";
    public static final String MEMBER = "member";
    public static final String PEMUKIMAN = "pemukiman";
    public static final String PENDIDIKAN = "pendidikan";
    public static final String POLSEK = "polsek";
    public static final String PU = "pu";
    public static final String SOSIAL = "sosial";
    public static final String ADMIN = "administrator";

    public static String UserTypeToTopik(int userType){

        String topik = null;

        switch (userType){
            case User.TYPE_BUPATI:
                topik = "bupati";
                break;
            case User.TYPE_KESEHATAN:
                topik = "kesehatan";
                break;
            case User.TYPE_MEMBER:
                topik = "member";
                break;
            case User.TYPE_PEMUKIMAN:
                topik = "pemukiman";
                break;
            case User.TYPE_PENDIDIKAN:
                topik = "pendidikan";
                break;
            case User.TYPE_POLSEK:
                topik = "polsek";
                break;
            case User.TYPE_PU:
                topik = "pu";
                break;
            case User.TYPE_SOSIAL:
                topik = "sosial";
                break;
            case User.TYPE_ADMINISTRATOR:
                topik = "admin";
                break;
        }

        return topik;
    }
}
