package app.ezza.laporpengaduan.model;

import java.io.Serializable;

/**
 * Created by azisa on 7/15/2017.
 */

public class Chat implements Serializable{

    public String sender;
    public String receiver;
    public String senderUid;
    public String receiverUid;
    public String message;
    public boolean isRead;
    public long timestamp;

    public Chat() {}

    /**
     * @param sender username
     * @param receiver username
     * @param senderUid sender uid
     * @param receiverUid receiver uid
     * @param message text message
     * @param timestamp set current timestamp
     */
    public Chat(String sender, String receiver, String senderUid, String receiverUid, String message, boolean isRead, long timestamp) {
        this.sender = sender;
        this.receiver = receiver;
        this.senderUid = senderUid;
        this.receiverUid = receiverUid;
        this.message = message;
        this.isRead = isRead;
        this.timestamp = timestamp;
    }

}
