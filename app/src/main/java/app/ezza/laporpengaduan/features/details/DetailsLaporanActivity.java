package app.ezza.laporpengaduan.features.details;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseException;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.DialogUtils;

public class DetailsLaporanActivity extends AppCompatActivity implements OnMapReadyCallback, DetailsLaporanComponent, View.OnClickListener {

    public static final String KEY_INTENT_LAPOR = "lapor";
    private static final String TAG = DetailsLaporanActivity.class.getSimpleName();
    private Lapor mLapor;
    private GoogleMap mMap;
    private LatLng mLatLng;
    private ImageView imgLapor;
    private TextView textDesc;
    private DetailsLaporanPresenter presenter;
    private TextView textNamaPengirim;
    private TextView textEmailPengirim;
    private Pref pref;
    private ProgressDialog progressDialog;
    private TextView textStatus;
    private ImageView imgStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_laporan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        presenter = new DetailsLaporanPresenter(this, this);

        handleIntent(getIntent());

        renderView();

        presenter.loadDataPengirim(mLapor.getSenderUid());

    }

    private void handleIntent(Intent intent) {
        mLapor = (Lapor)intent.getSerializableExtra(KEY_INTENT_LAPOR);
        mLatLng = new LatLng(mLapor.getLatitude(), mLapor.getLongitude());
    }

    private void renderView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        imgLapor = (ImageView) findViewById(R.id.img_lapor);
        imgStatus = (ImageView) findViewById(R.id.img_status);
        textDesc = (TextView) findViewById(R.id.text_desc_detail);

        textNamaPengirim = (TextView) findViewById(R.id.text_nama_lengkap);
        textEmailPengirim = (TextView) findViewById(R.id.text_email);
        textStatus = (TextView) findViewById(R.id.text_status_detail);

        Transformation transformation = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = imgLapor.getWidth();

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };

        Picasso.with(this)
                .load(mLapor.getImgUrl())
                .transform(transformation)
                .into(imgLapor);

        if(mLapor.getEksekusi() != null)
            Picasso.with(this)
                    .load(mLapor.getEksekusi().getImgurl())
                    .transform(transformation)
                    .into(imgStatus);
        else
            imgStatus.setVisibility(View.GONE);

        textDesc.setText(mLapor.getDesc());

        initView();

    }

    private void initView() {
        setStatusLaporan(mLapor);
    }

    private void setStatusLaporan(Lapor mLapor) {
        if(mLapor.getStatus().equals(Lapor.STATUS_ON_PROCESS)){
            textStatus.setText(getString(R.string.laporan_proses));
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_ACCEPT)){
            textStatus.setText(getString(R.string.laporan_terima));
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_SUCCESS)){
            String textEksek = getString(R.string.laporan_eksekusi);
            textEksek = textEksek + "\n\n" + mLapor.getEksekusi().getKeterangan();
            textStatus.setText(textEksek);
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_CANCEL)){
            String textTolak = getString(R.string.laporan_tolak);
            textTolak = textTolak +"\n\n"+ mLapor.getTolak();
            textStatus.setBackgroundResource(R.color.md_red_200);
            textStatus.setText(textTolak);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
        mMap.addMarker(new MarkerOptions().position(mLatLng));
    }

    @Override
    public void onSuccessLoadDataPengirim(String key, User user) {
        textNamaPengirim.setText(user.getNama());
        textEmailPengirim.setText(user.getEmail());
    }

    @Override
    public void onErrorLoadDataPengirim(DatabaseException e) {
        closeProgressLoading();
        if(!DetailsLaporanActivity.this.isFinishing())
            DialogUtils.getInstance().showErrorRetryDialog(this, e.getMessage(), new DialogUtils.ErrorRetryListener() {
                @Override
                public void onDone(DialogInterface dialogInterface, int position) {
                    dialogInterface.dismiss();
                }

                @Override
                public void onRetry(DialogInterface dialogInterface, int position) {
                    presenter.loadDataPengirim(mLapor.getSenderUid());
                }
            });
    }

    @Override
    public void onSuccessUpdateStatus(Lapor lapor) {
        closeProgressLoading();
        DialogUtils.getInstance().showInfoDialog(this, "Informasi", "Laporan berhasil di " + lapor.getStatus(), new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                dialogInterface.dismiss();
            }
        });

        setStatusLaporan(lapor);

    }

    @Override
    public void onErrorUpdateStatus(Exception e) {
        closeProgressLoading();
        DialogUtils.getInstance().showInfoDialog(this, "Error", e.getMessage(), new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_terima){
            DialogUtils.getInstance().showYesNoDialog(this, "Konfirmasi status", "Perubahan status tdk dapat di edit kembali. Yakin ingin merubah status laporan ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionTerima();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });

        }else if(view.getId() == R.id.btn_eksekusi){
            DialogUtils.getInstance().showYesNoDialog(this, "Konfirmasi status", "Perubahan status tdk dapat di edit kembali. Yakin ingin merubah status laporan ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionEksekusi();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });
        }else if(view.getId() == R.id.btn_tolak){
            DialogUtils.getInstance().showYesNoDialog(this, "Konfirmasi status", "Perubahan status tdk dapat di edit kembali. Yakin ingin merubah status laporan ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionTolak();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });
        }
    }

    private void doActionTolak() {
        showProgressLoading();
        presenter.updateStatusLaporan(mLapor, Lapor.STATUS_ON_CANCEL);
    }

    private void doActionEksekusi() {
        showProgressLoading();
        presenter.updateStatusLaporan(mLapor, Lapor.STATUS_ON_SUCCESS);
    }

    private void doActionTerima() {
        showProgressLoading();
        presenter.updateStatusLaporan(mLapor, Lapor.STATUS_ON_ACCEPT);
    }

    private void showProgressLoading(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void closeProgressLoading(){
        if(progressDialog != null)
            progressDialog.dismiss();
    }
}
