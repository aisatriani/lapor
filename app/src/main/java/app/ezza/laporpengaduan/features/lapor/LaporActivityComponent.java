package app.ezza.laporpengaduan.features.lapor;

import android.location.Location;

import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 7/1/2017.
 */

public interface LaporActivityComponent {
    void onLocationResult(Location location);

    void onSuccessSubmitLapor(Lapor lapor);

    void onError(Exception e);

    void onSuccessSendNotification(String result);

    void onErrorSendNotification(Throwable e);
}
