package app.ezza.laporpengaduan.features.main.fragment.info;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.Info;

/**
 * Created by azisa on 6/28/2017.
 */

public interface InfoFragmentComponent {
    void onSuccessLoadInfo(List<Info> value);

    void onError(DatabaseException e);
}
