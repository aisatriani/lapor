package app.ezza.laporpengaduan.features.auth.register;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 6/26/2017.
 */

public interface RegisterComponent {
    void onSuccessRegistration(AuthResult authResult, User users);

    void onError(Exception e);

    void onComplete(Task<AuthResult> task);

    void onUpdateAkun(User users);
}
