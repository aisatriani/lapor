package app.ezza.laporpengaduan.features.admin.laporan;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.features.auth.login.LoginPresenter;
import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 8/19/2017.
 */

public class LaporanAdminPresenter {
    private static final String TAG = LaporanAdminPresenter.class.getSimpleName();
    private Context context;
    private LaporanAdminComponent viewComponent;

    public LaporanAdminPresenter(Context context, LaporanAdminComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
    }


    public void loadChartData(String mKategori) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
        databaseReference.orderByChild("topikSubscriber").startAt(mKategori.toLowerCase()).endAt(mKategori.toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                long countEksekusi = 0;
                long countTolak = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Lapor lapor = ds.getValue(Lapor.class);
                    if(lapor.getStatus().equals(Lapor.STATUS_ON_SUCCESS)){
                        countEksekusi = countEksekusi + 1;
                    }
                    if(lapor.getStatus().equals(Lapor.STATUS_ON_CANCEL)){
                        countTolak = countTolak + 1;
                    }
                }

                Log.e(TAG, "onDataChange: count eksekusi " + countEksekusi );
                Log.e(TAG, "onDataChange: count tolak " + countTolak );

                viewComponent.onResultCountDataChart(countEksekusi,countTolak);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onErrorCountDataChart(databaseError.toException());
            }
        });

    }
}
