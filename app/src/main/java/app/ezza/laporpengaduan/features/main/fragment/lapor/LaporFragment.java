package app.ezza.laporpengaduan.features.main.fragment.lapor;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.lapor.LaporActivity;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LaporFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LaporFragment extends Fragment implements LaporFragmentComponent, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LaporFragmentPresenter presenter;
    private ImageView imgPreview;
    private Button btnPick;
    private LinearLayout layoutButtonLapor;
    private Uri mImageURI;


    public LaporFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LaporFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LaporFragment newInstance(String param1, String param2) {
        LaporFragment fragment = new LaporFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        presenter = new LaporFragmentPresenter(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_lapor, container, false);
        renderView(view);
        return view;
    }

    private void renderView(View view) {
        imgPreview = (ImageView) view.findViewById(R.id.img_preview);
        btnPick = (Button) view.findViewById(R.id.btn_pick);
        btnPick.setOnClickListener(this);

        layoutButtonLapor = (LinearLayout) view.findViewById(R.id.layout_button_lapor);
        view.findViewById(R.id.btn_repick).setOnClickListener(this);
        view.findViewById(R.id.btn_lapor).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_pick){
            RxImagePicker.with(getActivity())
                    .requestImage(Sources.CAMERA)
                    .subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) throws Exception {

                            mImageURI = uri;

                            Picasso.with(getActivity())
                                    .load(uri)
                                    .into(imgPreview);
                            btnPick.setVisibility(View.GONE);
                            layoutButtonLapor.setVisibility(View.VISIBLE);
                        }
                    });
        }

        if(view.getId() == R.id.btn_repick){
            RxImagePicker.with(getActivity())
                    .requestImage(Sources.CAMERA)
                    .subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) throws Exception {
                            Picasso.with(getActivity())
                                    .load(uri)
                                    .into(imgPreview);
                            btnPick.setVisibility(View.GONE);
                            layoutButtonLapor.setVisibility(View.VISIBLE);
                        }
                    });
        }
        if(view.getId() == R.id.btn_lapor){
           //showDialogLapor();
            Intent intent = new Intent(getActivity(), LaporActivity.class);
            intent.putExtra("image_uri",mImageURI.toString());
            startActivity(intent);
        }
    }

    private void showDialogLapor() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.layout_dialog_lapor)
                .setPositiveButton("LAPOR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();

        alertDialog.show();

        Spinner spinnerInstansi = (Spinner) alertDialog.findViewById(R.id.spinner_instansi);
        EditText editDesc = (EditText) alertDialog.findViewById(R.id.desc_lapor);


        List<String> data = new ArrayList<>();
        data.add("PENDIDIKAN");
        data.add("KESEHATAN");
        data.add("PU");
        data.add("PEMUKIMAN");
        data.add("POLSEK");
        data.add("SOSIAL");
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerInstansi.setAdapter(spinnerArrayAdapter);

        Button btnLapor = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btnLapor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
