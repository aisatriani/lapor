package app.ezza.laporpengaduan.features.admin.main.fragment.chat;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.ActiveChat;

/**
 * Created by azisa on 7/18/2017.
 */

public interface ChatFragmentComponent {
    void onSuccessLoadActiveChat(List<ActiveChat> activeChats);

    void onErrorLoadActiveChat(DatabaseException e);
}
