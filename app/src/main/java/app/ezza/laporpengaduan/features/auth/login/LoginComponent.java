package app.ezza.laporpengaduan.features.auth.login;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 6/26/2017.
 */

public interface LoginComponent {
    void onSuccessLogin(AuthResult authResult, User value);

    void onError(Exception e);

    void onCompleted();

    void onTokenReceived(String token);
}
