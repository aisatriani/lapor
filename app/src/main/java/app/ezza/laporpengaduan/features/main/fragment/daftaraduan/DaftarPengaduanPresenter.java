package app.ezza.laporpengaduan.features.main.fragment.daftaraduan;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 7/7/2017.
 */

public class DaftarPengaduanPresenter {

    private static final String TAG = DaftarPengaduanPresenter.class.getSimpleName();
    private final DatabaseReference databaseReference;
    private Context context;
    private DaftarPengaduanComponent viewComponent;
    private Pref pref;

    public DaftarPengaduanPresenter(Context context, DaftarPengaduanComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
        pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Pref.REF_LAPOR);
    }

    public void loadMyDataPengaduan() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Lapor> laporList = new ArrayList<Lapor>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Lapor lapor = ds.getValue(Lapor.class);
                    lapor.setUid(ds.getKey());
                    if(!lapor.getStatus().equals(Lapor.STATUS_ON_CANCEL))
                        laporList.add(lapor);
                }
                Collections.reverse(laporList);
                viewComponent.onSuccessMyDataPengaduan(laporList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onError(databaseError.toException());
            }
        });
    }
}
