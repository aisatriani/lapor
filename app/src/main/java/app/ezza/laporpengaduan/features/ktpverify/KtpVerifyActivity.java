package app.ezza.laporpengaduan.features.ktpverify;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import app.ezza.laporpengaduan.R;

public class KtpVerifyActivity extends AppCompatActivity {

    public static final String INTENT_STATUS = "intent.status";
    public static final int INT_KTP_VERIFY = 1;
    public static final int INT_KTP_CANCEL = 2;
    private TextView tvStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ktp_verify);

        tvStatus = (TextView)findViewById(R.id.txtStatus);
        int intExtra = getIntent().getIntExtra(INTENT_STATUS, 0);
        if(intExtra == INT_KTP_VERIFY){
            tvStatus.setText("Status akun anda belum aktiv. Pihak kami akan memverifikasi no KTP anda. Apabila sesuai kami akan mengirimkan email pemberitahuan bahwa akun anda telah aktiv.");
        }
        if (intExtra == INT_KTP_CANCEL){
            tvStatus.setText("Pendaftaran anda di tolak karena data anda tidak sesuai. Silahkan hub operator di kantor pelayanan wajib untuk memverifikasi langsung data anda. \n\nTerima kasih telah berpartisipasi dalam membangun daerah khususnya di kabupaten gorontalo.");
        }

    }
}
