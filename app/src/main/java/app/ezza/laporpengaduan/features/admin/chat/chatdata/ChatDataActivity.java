package app.ezza.laporpengaduan.features.admin.chat.chatdata;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.adapter.ChatAdapter;
import app.ezza.laporpengaduan.model.Chat;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;

public class ChatDataActivity extends AppCompatActivity implements View.OnClickListener, ChatDataComponent {

    public static final String KEY_INTENT_USER = "INTENT_USER";
    private RecyclerView rvChatData;
    private Pref pref;
    private ImageButton imgSend;
    private EditText tvMessage;
    private ChatDataPresenter presenter;
    private User mTargetUser;
    private List<Chat> chatList;
    private ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_data);

        presenter = new ChatDataPresenter(this, this);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        chatList = new ArrayList<>();

        handleIntent(getIntent());

        renderView();
    }

    private void handleIntent(Intent intent) {
        mTargetUser = (User) intent.getSerializableExtra(KEY_INTENT_USER);
        setTitle(mTargetUser.getNama());
    }

    private void renderView() {
        rvChatData = (RecyclerView) findViewById(R.id.rv_chat_data);
        rvChatData.setLayoutManager(new LinearLayoutManager(this));

        imgSend = (ImageButton) findViewById(R.id.img_send);
        imgSend.setOnClickListener(this);
        tvMessage = (EditText) findViewById(R.id.tv_send_message);

        adapter = new ChatAdapter(this, chatList);
        rvChatData.setAdapter(adapter);

        loadChatData();


    }

    private void loadChatData() {

        presenter.loadChatData(pref.getUser().getUid(), mTargetUser.getUid());

//        List<Chat> chatList = sampleDataChat();


    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.img_send){
            if(validate()){
                sendMessage();
            }else {
                Toast.makeText(this, "not sent message", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendMessage() {

        Chat chat = new Chat(
                pref.getUser().getNama(),
                mTargetUser.getNama(),
                pref.getUser().getUid(),
                mTargetUser.getUid(),
                tvMessage.getText().toString(),
                false,
                System.currentTimeMillis()
        );

        presenter.sendMessage(mTargetUser, chat);

        presenter.sendNotificationChat(mTargetUser, chat);

        tvMessage.setText("");

    }

    private boolean validate() {
        EditText[] editTexts = {tvMessage};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                //editText.setError("Tidak boleh kosong");
                return false;
            }
        }

        return true;
    }

    @Override
    public void onNewMessage(Chat chat) {
        adapter.addMessage(chat);
        rvChatData.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void onSuccessSendNotification(String response) {

    }

    @Override
    public void onErrorSendNotification(Throwable e) {

    }

    @Override
    public void onBackPressed() {
        presenter.removeListener();
        super.onBackPressed();
    }
}
