package app.ezza.laporpengaduan.features.auth.login;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.main.MainAdminActivity;
import app.ezza.laporpengaduan.features.admin.main.MainAdminTabActivity;
import app.ezza.laporpengaduan.features.auth.register.RegisterActivity;
import app.ezza.laporpengaduan.features.ktpverify.KtpVerifyActivity;
import app.ezza.laporpengaduan.features.main.MainActivity;
import app.ezza.laporpengaduan.model.Info;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;

public class LoginActivity extends AppCompatActivity implements LoginComponent, View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private LoginPresenter presenter;
    private LinearLayout layoutBottom;
    private TextInputEditText editEmail;
    private TextInputEditText editPassword;
    private TextView textBuatAkun;
    private ProgressDialog pd;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
        presenter = new LoginPresenter(this);

        renderView();
    }

    private void renderView() {

        layoutBottom = (LinearLayout) findViewById(R.id.layout_bottom);
        editEmail = (TextInputEditText) findViewById(R.id.edit_email);
        editPassword = (TextInputEditText) findViewById(R.id.edit_password);
        findViewById(R.id.btn_login).setOnClickListener(this);
        textBuatAkun = (TextView) findViewById(R.id.text_buat_akun);
        textBuatAkun.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_login){

            if(validate()){
                submitLogin();
            }

        }else if(view.getId()== R.id.text_buat_akun){
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        }

    }

    private void submitLogin() {

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        presenter.login(editEmail.getText().toString(), editPassword.getText().toString());


    }

    private boolean validate() {
        EditText[] editTexts = {editEmail, editPassword};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }

        String email = editEmail.getText().toString();
        if(!email.matches(Patterns.EMAIL_ADDRESS.pattern())){
            editEmail.setError("format email tidak benar");
            editEmail.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onSuccessLogin(AuthResult authResult, User user) {
        pref.storeUser(user);
        pref.setLogin(true);
        Log.d(TAG, "onSuccessLogin: login success");

        if(pd != null)
            pd.dismiss();

        FcmUtils.registerTopik(this);

        if(user.isKtpVerify()){
            if(user.getType() == User.TYPE_MEMBER){
                Log.d(TAG, "onSuccessLogin: start activity type member");
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
                return;
            }else{
                Intent intent = new Intent(this, MainAdminTabActivity.class);
                startActivity(intent);
                this.finish();
            }
        }else {
            Intent intent = new Intent(LoginActivity.this, KtpVerifyActivity.class);
            intent.putExtra(KtpVerifyActivity.INTENT_STATUS, KtpVerifyActivity.INT_KTP_VERIFY);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    @Override
    public void onError(Exception e) {

        if(pd != null)
            pd.dismiss();

        if(e instanceof FirebaseAuthInvalidUserException){
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Error")
                    .setMessage("Email atau password tidak benar. harap periksa kembali")
                    .setNegativeButton("Selesai", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create().show();
        }else if(e instanceof DatabaseException){

            new AlertDialog.Builder(getApplicationContext())
                    .setCancelable(false)
                    .setTitle("Error")
                    .setMessage(e.getMessage())
                    .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            submitLogin();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton("Selesai", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create().show();

        }


    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onTokenReceived(String token) {
        Log.d(TAG, "onTokenReceived: "+ token);
        pref.storeToken(token);
    }
}
