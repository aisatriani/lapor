package app.ezza.laporpengaduan.features.admin.laporan;

import com.google.firebase.database.DatabaseException;

/**
 * Created by azisa on 8/19/2017.
 */

public interface LaporanAdminComponent {
    void onResultCountDataChart(long countEksekusi, long countTolak);

    void onErrorCountDataChart(DatabaseException e);
}
