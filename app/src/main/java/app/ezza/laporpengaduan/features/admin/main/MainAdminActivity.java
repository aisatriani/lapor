package app.ezza.laporpengaduan.features.admin.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.info.InfoAdminActivity;
import app.ezza.laporpengaduan.features.splash.SplashActivity;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.utils.DialogUtils;
import app.ezza.laporpengaduan.utils.FcmUtils;

public class MainAdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String TAG = MainAdminActivity.class.getSimpleName();
    private TextInputEditText editTitleInfo;
    private TextInputEditText editContentInfo;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME,MODE_PRIVATE));

        renderView();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        loadSpesifikLaporan();
    }

    private void renderView() {

    }

    private void loadSpesifikLaporan() {
        FirebaseDatabase instance = FirebaseDatabase.getInstance();

        DatabaseReference reference = instance.getReference(Pref.REF_LAPOR);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Lapor value = dataSnapshot1.getValue(Lapor.class);
                    if(value.getTopikSubscriber().equals(pref.getUser().getType())){
                        Log.d(TAG, "onDataChange: "+ value.getTopikSubscriber());
                        Log.d(TAG, "onDataChange: "+ value.getDesc());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_info) {
            Intent intent = new Intent(this, InfoAdminActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_aduan) {

        } else if (id == R.id.nav_pesan) {

        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_logout) {
            DialogUtils.getInstance().showYesNoDialog(this, "Logout Konfirmasi", "Yakin ingin keluar dari pengguna aplikasi ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionLogout();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void doActionLogout() {
        FirebaseAuth.getInstance().signOut();
        FcmUtils.unregisterTopik(this);
        pref.clear();
        FirebaseDatabase.getInstance().goOffline();

        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(i);
    }

    @Override
    public void onClick(View view) {

    }
}
