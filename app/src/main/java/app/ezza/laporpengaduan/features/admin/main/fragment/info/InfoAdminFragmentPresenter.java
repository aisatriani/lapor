package app.ezza.laporpengaduan.features.admin.main.fragment.info;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.model.Info;

/**
 * Created by azisa on 6/28/2017.
 */

public class InfoAdminFragmentPresenter {

    private static final String TAG = InfoAdminFragmentPresenter.class.getSimpleName();
    private final DatabaseReference infoReference;
    private Context context;
    private InfoAdminFragmentComponent component;
    private FirebaseDatabase firebaseDatabase;

    public InfoAdminFragmentPresenter(Context context, InfoAdminFragmentComponent component) {
        this.context = context;
        this.component = component;
        firebaseDatabase = FirebaseDatabase.getInstance();
        infoReference = firebaseDatabase.getReference("info");
    }

    public void loadInfo() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();
        if(currentUser != null)
            infoReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onDataChange: data count " + dataSnapshot.getChildrenCount());
                    List<Info> infoList = new ArrayList<Info>();
                    for(DataSnapshot infoSnapshot : dataSnapshot.getChildren()){
                        String uid = infoSnapshot.getKey();
                        Info value = infoSnapshot.getValue(Info.class);
                        infoList.add(value);
                    }

                    component.onSuccessLoadInfo(infoList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    component.onError(databaseError.toException());
                }


            });
    }
}
