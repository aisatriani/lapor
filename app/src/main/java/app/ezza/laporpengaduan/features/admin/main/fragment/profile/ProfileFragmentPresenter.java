package app.ezza.laporpengaduan.features.admin.main.fragment.profile;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/26/2017.
 */

public class ProfileFragmentPresenter {
    private Context context;
    private ProfileFragmentComponent viewComponent;
    private Pref pref;

    public ProfileFragmentPresenter(Context context, ProfileFragmentComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
        this.pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    public void uploadProfilePic(final User mUser, Uri imageUri) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference reference = firebaseStorage.getReference("profile/"+imageUri.getLastPathSegment());
        reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                mUser.setImageUrl(downloadUrl.toString());
                updateUser(mUser);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@android.support.annotation.NonNull Exception e) {
                e.printStackTrace();
                System.out.println("error upload image to firebase");
                viewComponent.onUploadProfileError(e);
            }
        });
    }

    private void updateUser(final User mUser) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(Pref.REF_USERS);
        databaseReference.child(mUser.getUid()).setValue(mUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                pref.storeUser(mUser);
                viewComponent.onSuccessUploadProfile(mUser);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onUploadProfileError(e);
            }
        });
    }
}
