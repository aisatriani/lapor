package app.ezza.laporpengaduan.features.lapor;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.patloew.rxlocation.RxLocation;

import org.json.JSONException;

import java.io.IOException;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.utils.FcmUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by azisa on 7/1/2017.
 */

public class LaporActivityPresenter {
    private static final String TAG = LaporActivityPresenter.class.getSimpleName();
    private Context context;
    private LaporActivityComponent viewComponent;

    public LaporActivityPresenter(Context context, LaporActivityComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
    }

    public void loadCurrentLocation() {

        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(2)
                .setInterval(1000);

        RxLocation rxLocation = new RxLocation(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "loadCurrentLocation: permission location denied");
            return;
        }
        rxLocation.location().updates(request)
                .subscribe(new Consumer<Location>() {
                    @Override
                    public void accept(@NonNull Location location) throws Exception {
                        Log.d(TAG, "accept: location on result successfully");
                        viewComponent.onLocationResult(location);
                    }
                });


    }

    public void submitLapor(final Lapor lapor, Uri imageUri) {

        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference reference = firebaseStorage.getReference("lapor/"+imageUri.getLastPathSegment());
        reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                lapor.setImgUrl(downloadUrl.toString());
                storeLapor(lapor);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@android.support.annotation.NonNull Exception e) {
                e.printStackTrace();
                System.out.println("error upload image to firebase");
                viewComponent.onError(e);
            }
        });

    }

    private void storeLapor(final Lapor lapor) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(Pref.REF_LAPOR);
        databaseReference.push().setValue(lapor).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                viewComponent.onSuccessSubmitLapor(lapor);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@android.support.annotation.NonNull Exception e) {
                viewComponent.onError(e);
            }
        });
    }

    public void sendNotificationLaporTopik(final Lapor lapor) {
        Observable<String> observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                Gson gson = new Gson();
                String json = gson.toJson(lapor);
                try {
                    String response = FcmUtils.sendMessageToTopik(lapor.getTopikSubscriber(), Pref.REF_LAPOR, json);
                    e.onNext(response);
                    e.onComplete();
                }catch (IOException ioex){
                    e.onError(ioex);
                }

            }
        });

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new DefaultObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        viewComponent.onSuccessSendNotification(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewComponent.onErrorSendNotification(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
