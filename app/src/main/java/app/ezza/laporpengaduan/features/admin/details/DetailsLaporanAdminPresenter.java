package app.ezza.laporpengaduan.features.admin.details;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.IOException;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Eksekusi;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by azisa on 7/14/2017.
 */

public class DetailsLaporanAdminPresenter {

    private Context context;
    private DetailsLaporanAdminComponent viewComponent;
    private User mUser;

    public DetailsLaporanAdminPresenter(Context context, DetailsLaporanAdminComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
    }

    public void loadDataPengirim(String uid) {

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.getReference(Pref.REF_USERS).child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                User user = dataSnapshot.getValue(User.class);
                viewComponent.onSuccessLoadDataPengirim(key,user);
                mUser = user;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onErrorLoadDataPengirim(databaseError.toException());
            }
        });

    }

    public void updateStatusLaporan(final Lapor mLapor, final String status) {
        mLapor.setStatus(status);
        DatabaseReference laporRef = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
        laporRef.child(mLapor.getUid()).setValue(mLapor).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                viewComponent.onSuccessUpdateStatus(mLapor);
                if(mUser != null){
                    sendNotificationUpdateStatus(mLapor);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onErrorUpdateStatus(e);
            }
        });

    }

    private void sendNotificationUpdateStatus(final Lapor lapor) {
        Observable<String> observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<String> e) throws Exception {
                Gson gson = new Gson();
                String json = gson.toJson(lapor);
                try {
                    String response = FcmUtils.sendMessageToDevice(mUser.getTokenFcm(),Pref.NOTIFICATION_LAPOR_FEEDBACK, json);
//                    String response = FcmUtils.sendMessageToTopik(lapor.getTopikSubscriber(), Pref.REF_LAPOR, json);
                    e.onNext(response);
                    e.onComplete();
                }catch (IOException ioex){
                    e.onError(ioex);
                }

            }
        });

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new DefaultObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        viewComponent.onSuccessSendNotification(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewComponent.onErrorSendNotification(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void eksekusiLaporan(Uri imgUri, final String keterangan, final Lapor mLapor, String statusOnSuccess) {
        mLapor.setStatus(statusOnSuccess);

        StorageReference storageReference = FirebaseStorage.getInstance().getReference("eksekusi/" + imgUri.getLastPathSegment());
        storageReference.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Eksekusi eksekusi = new Eksekusi();
                eksekusi.setImgurl(downloadUrl.toString());
                eksekusi.setKeterangan(keterangan);
                mLapor.setEksekusi(eksekusi);

                DatabaseReference laporRef = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
                laporRef.child(mLapor.getUid()).setValue(mLapor).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        viewComponent.onSuccessUpdateStatus(mLapor);
                        if(mUser != null){
                            sendNotificationUpdateStatus(mLapor);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        viewComponent.onErrorUpdateStatus(e);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onErrorUpdateStatus(e);
            }
        });


    }

    public void tolakLaporan(String keterangan, final Lapor mLapor, String statusOnCancel) {

        mLapor.setStatus(statusOnCancel);
        mLapor.setTolak(keterangan);

        DatabaseReference laporRef = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
        laporRef.child(mLapor.getUid()).setValue(mLapor).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                viewComponent.onSuccessUpdateStatus(mLapor);
                if(mUser != null){
                    sendNotificationUpdateStatus(mLapor);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onErrorUpdateStatus(e);
            }
        });
    }
}
