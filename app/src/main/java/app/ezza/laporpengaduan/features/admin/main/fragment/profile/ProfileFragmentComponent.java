package app.ezza.laporpengaduan.features.admin.main.fragment.profile;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/26/2017.
 */

public interface ProfileFragmentComponent {
    void onUploadProfileError(Exception e);

    void onSuccessUploadProfile(User mUser);
}
