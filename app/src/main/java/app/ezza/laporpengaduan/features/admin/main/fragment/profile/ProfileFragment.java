package app.ezza.laporpengaduan.features.admin.main.fragment.profile;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.CircleTransform;
import app.ezza.laporpengaduan.utils.DialogUtils;
import app.ezza.laporpengaduan.utils.FcmUtils;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements ProfileFragmentComponent, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProfileFragmentPresenter presenter;
    private ImageView imgProfile;
    private TextView textProfileFullname;
    private TextView textProfileEmail;
    private TextView textProfileTelp;
    private Uri mImageURI;
    private Pref pref;
    private User mUser;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        presenter = new ProfileFragmentPresenter(getActivity(), this);
        pref = new Pref(getActivity().getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        mUser = pref.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_admin, container, false);
        renderView(view);
        return view;
    }

    private void renderView(View view) {
        imgProfile = (ImageView) view.findViewById(R.id.img_profile);
        imgProfile.setOnClickListener(this);
        textProfileFullname = (TextView) view.findViewById(R.id.text_profile_fullname);
        textProfileEmail = (TextView) view.findViewById(R.id.text_profile_email);
        textProfileTelp = (TextView) view.findViewById(R.id.text_profile_telp);
        view.findViewById(R.id.btn_logout).setOnClickListener(this);

        textProfileFullname.setText(mUser.getNama());
        textProfileEmail.setText(mUser.getEmail());
        textProfileTelp.setText(mUser.getTelp());

        setImageProfile();
    }

    private void setImageProfile() {
        if(mUser.getImageUrl() != null)
            Picasso.with(getActivity())
                    .load(mUser.getImageUrl())
                    .placeholder(R.drawable.ic_add_picx)
                    .transform(new CircleTransform())
                    .into(imgProfile);
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.img_profile){
            showImagePicker();
        }else if(view.getId() == R.id.btn_logout){
            DialogUtils.getInstance().showYesNoDialog(getActivity(), "Logout Konfirmasi", "Yakin ingin keluar dari pengguna aplikasi ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionLogout();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });
        }
    }

    private void showImagePicker() {
        RxImagePicker.with(getActivity())
                .requestImage(Sources.GALLERY)
                .subscribe(new Consumer<Uri>() {
                    @Override
                    public void accept(@NonNull Uri uri) throws Exception {

                        mImageURI = uri;

                        Picasso.with(getActivity())
                                .load(uri)
                                .transform(new CircleTransform())
                                .into(imgProfile);

                        presenter.uploadProfilePic(mUser, uri);
                    }
                });
    }


    private void doActionLogout() {
        FirebaseAuth.getInstance().signOut();
        FcmUtils.unregisterTopik(getActivity());
        pref.clear();
        FirebaseDatabase.getInstance().goOffline();

        Intent i = getActivity().getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(i);

        getActivity().finish();

    }

    @Override
    public void onUploadProfileError(Exception e) {
        e.printStackTrace();
        Toast.makeText(getActivity(), "Gagal mengupload gambar profile. "+ e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessUploadProfile(User mUser) {
        Toast.makeText(getActivity(), "Gambar profile berhasil diubah", Toast.LENGTH_SHORT).show();
    }
}
