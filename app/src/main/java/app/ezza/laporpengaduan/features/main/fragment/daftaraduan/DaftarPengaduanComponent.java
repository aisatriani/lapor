package app.ezza.laporpengaduan.features.main.fragment.daftaraduan;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 7/7/2017.
 */

public interface DaftarPengaduanComponent {
    void onSuccessMyDataPengaduan(List<Lapor> laporList);

    void onError(DatabaseException e);
}
