package app.ezza.laporpengaduan.features.admin.laporandetails;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 8/19/2017.
 */

public class LaporanDetailsPresenter {
    private static final String TAG = LaporanDetailsPresenter.class.getSimpleName();
    private Context context;
    private LaporanDetailsComponent viewConponent;

    public LaporanDetailsPresenter(Context context, LaporanDetailsComponent viewConponent) {
        this.context = context;
        this.viewConponent = viewConponent;
    }

    public void loadLaporan(String kategori, final String status) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
        databaseReference.orderByChild("topikSubscriber").startAt(kategori.toLowerCase()).endAt(kategori.toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<Lapor> laporList = new ArrayList<Lapor>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Lapor lapor = ds.getValue(Lapor.class);

                    if(lapor.getStatus().equals(status)){
                        laporList.add(lapor);
                    }
//                    if(lapor.getStatus().equals(Lapor.STATUS_ON_CANCEL)){
//                        laporList.add(lapor);
//                    }
                }

                viewConponent.onSuccessLoadData(laporList);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewConponent.onErrorLoadData(databaseError.toException());
            }
        });
    }
}
