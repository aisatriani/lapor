package app.ezza.laporpengaduan.features.main.fragment.lapor;

import android.content.Context;

/**
 * Created by azisa on 6/29/2017.
 */

public class LaporFragmentPresenter {

    private Context context;
    private LaporFragmentComponent viewComponent;

    public LaporFragmentPresenter(Context context, LaporFragmentComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
    }

}
