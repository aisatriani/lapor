package app.ezza.laporpengaduan.features.admin.main.fragment.daftaraduan;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 7/7/2017.
 */

public interface DaftarPengaduanAdminComponent {
    void onSuccessMyDataPengaduan(List<Lapor> laporList);

    void onError(DatabaseException e);

    void onDeleteSuccess(Lapor lapor);
}
