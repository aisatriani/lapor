package app.ezza.laporpengaduan.features.admin.chat.chatdata;

import app.ezza.laporpengaduan.model.Chat;

/**
 * Created by azisa on 7/16/2017.
 */

public interface ChatDataComponent {
    void onNewMessage(Chat chat);

    void onSuccessSendNotification(String response);

    void onErrorSendNotification(Throwable e);
}
