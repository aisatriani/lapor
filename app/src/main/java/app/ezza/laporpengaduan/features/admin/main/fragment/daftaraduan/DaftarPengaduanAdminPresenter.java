package app.ezza.laporpengaduan.features.admin.main.fragment.daftaraduan;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;

/**
 * Created by azisa on 7/7/2017.
 */

public class DaftarPengaduanAdminPresenter {

    private static final String TAG = DaftarPengaduanAdminPresenter.class.getSimpleName();
    private final DatabaseReference databaseReference;
    private Context context;
    private DaftarPengaduanAdminComponent viewComponent;
    private Pref pref;

    public DaftarPengaduanAdminPresenter(Context context, DaftarPengaduanAdminComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
        pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Pref.REF_LAPOR);
    }

    public void loadMyDataPengaduan() {
        String userTypeToTopik = Topik.UserTypeToTopik(pref.getUser().getType());
        databaseReference.orderByChild("topikSubscriber").startAt(userTypeToTopik).endAt(userTypeToTopik).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Lapor> laporList = new ArrayList<Lapor>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Lapor lapor = ds.getValue(Lapor.class);
                    lapor.setUid(ds.getKey());
                    laporList.add(lapor);
                }
                Collections.reverse(laporList);
                viewComponent.onSuccessMyDataPengaduan(laporList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onError(databaseError.toException());
            }
        });
    }

    public void loadPengaduanAdminBupati(){
        String userTypeToTopik = Topik.UserTypeToTopik(pref.getUser().getType());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Lapor> laporList = new ArrayList<Lapor>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Lapor lapor = ds.getValue(Lapor.class);
                    lapor.setUid(ds.getKey());
                    laporList.add(lapor);
                }
                Collections.reverse(laporList);
                viewComponent.onSuccessMyDataPengaduan(laporList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onError(databaseError.toException());
            }
        });
    }

    public void deleteLaporan(final Lapor lapor) {
        databaseReference.child(lapor.getUid()).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                viewComponent.onDeleteSuccess(lapor);
            }
        });
    }
}
