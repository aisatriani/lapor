package app.ezza.laporpengaduan.features.admin.chat.chatdata;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.IOException;

import app.ezza.laporpengaduan.BuildConfig;
import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Chat;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by azisa on 7/16/2017.
 */

public class ChatDataPresenter {
    private static final String TAG = ChatDataPresenter.class.getSimpleName();
    private ChatDataComponent viewComponent;
    private Context context;
    private Pref pref;
    private DatabaseReference chatRef2;
    private DatabaseReference chatRef1;
    private ChildEventListener childEventListener1;
    private ChildEventListener childEventListener2;

    public ChatDataPresenter(ChatDataComponent viewComponent, Context context) {
        this.viewComponent = viewComponent;
        this.context = context;
        this.pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    public void sendMessage(User mTargetUser, final Chat chat) {

        final String room_type_1 = chat.senderUid + "_" + chat.receiverUid;
        final String room_type_2 = chat.receiverUid + "_" + chat.senderUid;

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference();

        databaseReference.child(Pref.REF_CHAT)
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(room_type_1)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                            databaseReference.child(Pref.REF_CHAT)
                                    .child(room_type_1)
                                    .child(String.valueOf(chat.timestamp))
                                    .setValue(chat);
                        } else if (dataSnapshot.hasChild(room_type_2)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                            databaseReference.child(Pref.REF_CHAT)
                                    .child(room_type_2)
                                    .child(String.valueOf(chat.timestamp))
                                    .setValue(chat);
                        } else {
                            Log.e(TAG, "sendMessageToFirebaseUser: success");
                            databaseReference.child(Pref.REF_CHAT)
                                    .child(room_type_1)
                                    .child(String.valueOf(chat.timestamp))
                                    .setValue(chat);

                            loadChatData(chat.senderUid, chat.receiverUid);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Unable to send message.
                    }
                });
    }

    public void loadChatData(final String senderUid, final String receiverUid) {

        final String room_type_1 = senderUid + "_" + receiverUid;
        final String room_type_2 = receiverUid + "_" + senderUid;

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference();

        databaseReference.child(Pref.REF_CHAT)
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(room_type_1)) {
                            Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_1 + " type 1 exists");
                            chatRef1 = FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child(Pref.REF_CHAT)
                                    .child(room_type_1);

                            childEventListener1 = chatRef1
                                    .addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            // Chat message is retreived.
                                            Log.d(TAG, "onChildAdded: message retreived");
                                            String key = dataSnapshot.getKey();
                                            Chat chat = dataSnapshot.getValue(Chat.class);
                                            if (!chat.isRead)
                                                if (chat.receiverUid.equals(pref.getUser().getUid())) {
                                                    chat.isRead = true;
                                                    databaseReference.child(Pref.REF_CHAT)
                                                            .child(room_type_1)
                                                            .child(key)
                                                            .setValue(chat);
                                                    if (BuildConfig.DEBUG)
                                                        Log.e("ChatDataPresenter", "update is read type 1");
                                                }
                                            viewComponent.onNewMessage(chat);
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            // Unable to get message.
                                        }
                                    });

                        } else if (dataSnapshot.hasChild(room_type_2)) {
                            Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_2 + " type 2 exists");
                            chatRef2 = FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child(Pref.REF_CHAT)
                                    .child(room_type_2);

                            childEventListener2 = chatRef2
                                    .addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            // Chat message is retreived.
                                            Log.d(TAG, "onChildAdded: message retreived");
                                            String key = dataSnapshot.getKey();
                                            Chat chat = dataSnapshot.getValue(Chat.class);
                                            if (!chat.isRead)
                                                if (chat.receiverUid.equals(pref.getUser().getUid())) {
                                                    chat.isRead = true;
                                                    databaseReference.child(Pref.REF_CHAT)
                                                            .child(room_type_2)
                                                            .child(key)
                                                            .setValue(chat);

                                                    if (BuildConfig.DEBUG)
                                                        Log.e("ChatDataPresenter", "update is read type 2");
                                                }

                                            viewComponent.onNewMessage(chat);
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            // Unable to get message.
                                        }
                                    });
                        } else {
                            Log.e(TAG, "getMessageFromFirebaseUser: no such room available");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Unable to get message
                    }
                });

    }

    public void removeListener() {
        if(chatRef1 != null)
            chatRef1.removeEventListener(childEventListener1);
        if(chatRef2 != null)
            chatRef2.removeEventListener(childEventListener2);
    }

    public void sendNotificationChat(final User mTargetUser, final Chat chat) {
        Observable<String> observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                Gson gson = new Gson();
                String json = gson.toJson(chat);
                try {
                    String response = FcmUtils.sendMessageToTopik(Topik.UserTypeToTopik(mTargetUser.getType()), Pref.REF_CHAT, json);
                    e.onNext(response);
                    e.onComplete();
                }catch (IOException ioex){
                    e.onError(ioex);
                }
            }
        });

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new DefaultObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        viewComponent.onSuccessSendNotification(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewComponent.onErrorSendNotification(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
