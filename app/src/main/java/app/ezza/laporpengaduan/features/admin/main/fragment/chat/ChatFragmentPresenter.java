package app.ezza.laporpengaduan.features.admin.main.fragment.chat;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import app.ezza.laporpengaduan.BuildConfig;
import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.ActiveChat;
import app.ezza.laporpengaduan.model.Chat;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.TextUtils;

/**
 * Created by azisa on 7/18/2017.
 */

public class ChatFragmentPresenter {
    private static final String TAG = ChatFragmentPresenter.class.getSimpleName();
    private Context context;
    private ChatFragmentComponent viewComponent;
    private Pref pref;

    public ChatFragmentPresenter(Context context, ChatFragmentComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
        this.pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    public void loadActiveChat() {

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference(Pref.REF_CHAT);
        chatRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final List<ActiveChat> activeChats = new ArrayList<ActiveChat>();

                for(final DataSnapshot ds : dataSnapshot.getChildren()){

                    final String key = ds.getKey();

                    final ActiveChat activeChat = new ActiveChat();

                    String[] activeKey = key.split("_");
                    String senderUid = activeKey[0];
                    String receiverUid = activeKey[1];

                    Log.d(TAG, "senderuid: "+ senderUid + " prefUid: " + pref.getUser().getUid());

                    if(senderUid.equals(pref.getUser().getUid())){
                        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference(Pref.REF_USERS);
                        userRef.child(receiverUid).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                int countNewMessage = 0;
                                User dataUser = dataSnapshot.getValue(User.class);
                                activeChat.setName(dataUser.getNama());
                                activeChat.setUser(dataUser);

                                for (DataSnapshot valueSnapshot : ds.getChildren()){
                                    Chat chat = valueSnapshot.getValue(Chat.class);
                                    activeChat.setLastMessage(chat.message);
                                    activeChat.setDate(TextUtils.getFormattedDate(context,chat.timestamp));
                                    if(chat.receiverUid.equals(pref.getUser().getUid()))
                                        if(!chat.isRead){
                                            countNewMessage = countNewMessage + 1;
                                        }
                                }

                                activeChat.setNewMessage(countNewMessage);

                                activeChats.add(activeChat);
                                Collections.sort(activeChats, new Comparator<ActiveChat>() {
                                    @Override
                                    public int compare(ActiveChat activeChat, ActiveChat t1) {
                                        return activeChat.getNewMessage() - t1.getNewMessage();
                                    }
                                });

                                viewComponent.onSuccessLoadActiveChat(activeChats);

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                viewComponent.onErrorLoadActiveChat(databaseError.toException());
                            }
                        });
                    }else{
                        viewComponent.onSuccessLoadActiveChat(activeChats);
                    }

                    if(receiverUid.equals(pref.getUser().getUid())){
                        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference(Pref.REF_USERS);
                        userRef.child(senderUid).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                int countNewMessage = 0;
                                User dataUser = dataSnapshot.getValue(User.class);
                                activeChat.setName(dataUser.getNama());
                                activeChat.setUser(dataUser);

                                for (DataSnapshot valueSnapshot : ds.getChildren()){
                                    Chat chat = valueSnapshot.getValue(Chat.class);
                                    activeChat.setLastMessage(chat.message);
                                    activeChat.setDate(TextUtils.getFormattedDate(context,chat.timestamp));
                                    if(!chat.isRead){
                                        countNewMessage = countNewMessage + 1;
                                    }

                                }

                                activeChat.setNewMessage(countNewMessage);

                                activeChats.add(activeChat);
                                viewComponent.onSuccessLoadActiveChat(activeChats);

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                viewComponent.onErrorLoadActiveChat(databaseError.toException());
                            }
                        });
                    }else{
                        viewComponent.onSuccessLoadActiveChat(activeChats);
                    }

                    
                }



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ",databaseError.toException() );
                viewComponent.onErrorLoadActiveChat(databaseError.toException());
            }
        });

    }

    private User getActiveUser(String key) {
        String[] activeKey = key.split("_");
        String senderUid = activeKey[0];
        String receiverUid = activeKey[1];

        if(senderUid.equals(pref.getUser().getUid())){
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference(Pref.REF_USERS);
            userRef.child(receiverUid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User dataSnapshotValue = dataSnapshot.getValue(User.class);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        return null;
    }
}
