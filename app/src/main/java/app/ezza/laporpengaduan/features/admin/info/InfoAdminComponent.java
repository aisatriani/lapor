package app.ezza.laporpengaduan.features.admin.info;

/**
 * Created by azisa on 6/28/2017.
 */

public interface InfoAdminComponent {
    void onSuccessSubmit();

    void onError(Exception e);

    void onComplete();

    void onProgressVisibility(int visibility);
}
