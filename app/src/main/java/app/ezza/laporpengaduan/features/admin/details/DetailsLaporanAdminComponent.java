package app.ezza.laporpengaduan.features.admin.details;

import com.google.firebase.database.DatabaseException;

import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/14/2017.
 */

public interface DetailsLaporanAdminComponent {
    void onSuccessLoadDataPengirim(String key, User user);
    void onErrorLoadDataPengirim(DatabaseException e);

    void onSuccessUpdateStatus(Lapor mLapor);

    void onErrorUpdateStatus(Exception e);

    void onSuccessSendNotification(String s);

    void onErrorSendNotification(Throwable e);
}
