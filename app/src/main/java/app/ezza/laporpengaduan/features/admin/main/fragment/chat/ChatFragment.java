package app.ezza.laporpengaduan.features.admin.main.fragment.chat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.adapter.ActiveChatAdapter;
import app.ezza.laporpengaduan.features.admin.chat.chatdata.ChatDataActivity;
import app.ezza.laporpengaduan.model.ActiveChat;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment implements ChatFragmentComponent {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = ChatFragment.class.getSimpleName();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ChatFragmentPresenter presenter;
    private RecyclerView rvListChat;
    private ProgressBar progressBar;


    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        presenter = new ChatFragmentPresenter(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        renderView(view);
        return view;
    }

    private void renderView(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        rvListChat = (RecyclerView) view.findViewById(R.id.rv_list_chat);
        rvListChat.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.loadActiveChat();
    }

    @Override
    public void onSuccessLoadActiveChat(final List<ActiveChat> activeChats) {

        Log.d(TAG, "onSuccessLoadActiveChat: on success load");
        progressBar.setVisibility(View.GONE);

        ActiveChatAdapter adapter = new ActiveChatAdapter(getActivity(), activeChats);
        rvListChat.setAdapter(adapter);



        adapter.setClickListener(new ActiveChatAdapter.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                if(activeChats.get(position).getUser() == null){
                    Toast.makeText(getActivity(), "object intent user null", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(getActivity(), ChatDataActivity.class);
                intent.putExtra(ChatDataActivity.KEY_INTENT_USER, activeChats.get(position).getUser());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onErrorLoadActiveChat(DatabaseException e) {
        Log.d(TAG, "onErrorLoadActiveChat: error load chat");
        if(getActivity() != null)
            Toast.makeText(getActivity().getApplicationContext(), "Error: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
