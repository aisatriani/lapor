package app.ezza.laporpengaduan.features.admin.pengguna.edit;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.auth.login.LoginActivity;
import app.ezza.laporpengaduan.features.auth.register.RegisterComponent;
import app.ezza.laporpengaduan.features.auth.register.RegisterPresenter;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.DialogUtils;

public class EditAkunActivity extends AppCompatActivity implements RegisterComponent, View.OnClickListener {

    private static final String TAG = EditAkunActivity.class.getSimpleName();
    public static final String KEY_INTENT_USER = "key.user";
    private RegisterPresenter presenter;
    private LinearLayout layoutBottom;
    private TextInputEditText editUsername;
    private TextInputEditText editEmail;
    private TextInputEditText editTelp;
    private TextInputEditText editPassword;
    private TextInputEditText editRepassword;
    private TextView textLoginDisini;
    private ProgressDialog pd;
    private TextInputEditText editNamaLengkap;
    private Pref pref;
    private TextInputEditText editTypeUser;
    private int mTypeUser;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_operator);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
        presenter = new RegisterPresenter(this, this);

        mUser = (User) getIntent().getSerializableExtra(KEY_INTENT_USER);;

        renderView();
    }

    private void renderView() {

        layoutBottom = (LinearLayout) findViewById(R.id.layout_bottom);
        editNamaLengkap = (TextInputEditText) findViewById(R.id.edit_fullname);
        editUsername = (TextInputEditText) findViewById(R.id.edit_username);
        editEmail = (TextInputEditText) findViewById(R.id.edit_email);
        editTelp = (TextInputEditText) findViewById(R.id.edit_telp);
        editPassword = (TextInputEditText) findViewById(R.id.edit_password);
        editRepassword = (TextInputEditText) findViewById(R.id.edit_repassword);
        findViewById(R.id.btn_daftar).setOnClickListener(this);
        textLoginDisini = (TextView) findViewById(R.id.text_login_disini);
        textLoginDisini.setOnClickListener(this);
        editTypeUser = (TextInputEditText) findViewById(R.id.edit_type_user);
        editTypeUser.setInputType(InputType.TYPE_NULL);
        editTypeUser.setOnClickListener(this);

        editUsername.setEnabled(false);
        editEmail.setEnabled(false);

        //hide field password
        editPassword.setVisibility(View.GONE);
        editRepassword.setVisibility(View.GONE);

        setContentText();

    }

    private void setContentText() {
        editNamaLengkap.setText(mUser.getNama());
        editEmail.setText(mUser.getEmail());
        editTelp.setText(mUser.getTelp());
        editTypeUser.setText(Topik.UserTypeToTopik(mUser.getType()));
        editUsername.setText(mUser.getUsername());

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_daftar){

            if(validate()){
                //submitRegistration();
                updateAkun(mUser);

            }
        }else if (view.getId() == R.id.text_login_disini){
            onBackPressed();
        }else if(view.getId() == R.id.edit_type_user){
            showDialogTypeUser();
        }
    }

    private void updateAkun(User users) {
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        users.setNama(editNamaLengkap.getText().toString());
        users.setUsername(editUsername.getText().toString());
        users.setEmail(editEmail.getText().toString());
        users.setTelp(editTelp.getText().toString());
        users.setType(mTypeUser);
        presenter.updateAkun(users);
    }

    private void showDialogTypeUser() {

        final String[] data = getResources().getStringArray(R.array.kategori);

        new AlertDialog.Builder(this)
                .setItems(data, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        editTypeUser.setText(data[i]);
                        setTypeUser(data[i].toLowerCase());
                    }
                }).create().show();

    }

    private void setTypeUser(String type) {
        switch (type){
            case Topik.BUPATI:
                mTypeUser = User.TYPE_BUPATI;
                break;
            case Topik.KESEHATAN:
                mTypeUser = User.TYPE_KESEHATAN;
                break;
            case Topik.MEMBER:
                mTypeUser = User.TYPE_MEMBER;
                break;
            case Topik.PEMUKIMAN:
                mTypeUser = User.TYPE_PEMUKIMAN;
                break;
            case Topik.PENDIDIKAN:
                mTypeUser = User.TYPE_PENDIDIKAN;
                break;
            case Topik.POLSEK:
                mTypeUser = User.TYPE_POLSEK;
                break;
            case Topik.PU:
                mTypeUser = User.TYPE_PU;
                break;
            case Topik.SOSIAL:
                mTypeUser = User.TYPE_SOSIAL;
                break;
            case Topik.ADMIN:
                mTypeUser = User.TYPE_ADMINISTRATOR;
                break;
        }

        Log.d(TAG, "setTypeUser: "+ mTypeUser);
    }

    private void submitRegistration() {

        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        User users = new User();
        users.setNama(editNamaLengkap.getText().toString());
        users.setUsername(editUsername.getText().toString());
        users.setEmail(editEmail.getText().toString());
        users.setTelp(editTelp.getText().toString());
        users.setPassword(editPassword.getText().toString());
        users.setType(mTypeUser);
        users.setKtpVerify(true);
        presenter.submitRegistration(users);

    }

    private boolean validate() {
        EditText[] editTexts = {editNamaLengkap, editUsername, editEmail, editTypeUser, editTelp};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }


        String email = editEmail.getText().toString();
        if(!email.matches(Patterns.EMAIL_ADDRESS.pattern())){
            editEmail.setError("format email tidak benar");
            editEmail.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onSuccessRegistration(AuthResult authResult, User users) {
        pref.storeUser(users);
        Log.d(TAG, "onSuccessRegistration: registration complete user uid :" + users.getUid());

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Registration success")
                .setMessage("Selamat, anda telah berhasil mendaftar ke aplikasi ini. Silahkan login untuk masuk ke aplikasi")

                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(EditAkunActivity.this, LoginActivity.class);
                        startActivity(intent);
                        EditAkunActivity.this.finish();
                    }
                })
                .create().show();

    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();

        if(pd != null)
            pd.dismiss();

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Error")
                .setMessage(e.getMessage())
                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        submitRegistration();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();

    }

    @Override
    public void onComplete(Task<AuthResult> task) {

        if(pd != null)
            pd.dismiss();

    }

    @Override
    public void onUpdateAkun(User users) {
        pd.dismiss();
        DialogUtils.getInstance().showInfoDialog(this, "Berhasil", "Berhasil merubah akun", new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                dialogInterface.dismiss();
                EditAkunActivity.this.finish();
            }
        });
    }
}
