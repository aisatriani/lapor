package app.ezza.laporpengaduan.features.admin.laporandetails;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.adapter.DaftarAduanAdapter;
import app.ezza.laporpengaduan.model.Lapor;

public class LaporanAdminDetailsActivity extends AppCompatActivity implements LaporanDetailsComponent {

    private RecyclerView rvLapor;
    private LaporanDetailsPresenter presenter;
    private String mStatus;
    private ProgressDialog pd;
    private String mKategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_admin_details);

        presenter = new LaporanDetailsPresenter(this,this);

        mKategori = getIntent().getStringExtra("kategori");
        mStatus = getIntent().getStringExtra("status");

        rvLapor = (RecyclerView) findViewById(R.id.rv_lapor);
        rvLapor.setLayoutManager(new LinearLayoutManager(this));

        loadLaporanBykategori();
    }

    private void loadLaporanBykategori() {
        pd = new ProgressDialog(this);
        pd.setMessage("Memuat Data");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        presenter.loadLaporan(mKategori.toLowerCase(), mStatus.toLowerCase());
    }

    @Override
    public void onErrorLoadData(DatabaseException e) {
        pd.dismiss();
    }

    @Override
    public void onSuccessLoadData(List<Lapor> laporList) {
        pd.dismiss();
        DaftarAduanAdapter aduanAdapter = new DaftarAduanAdapter(this, laporList);
        rvLapor.setAdapter(aduanAdapter);
    }
}
