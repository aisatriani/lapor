package app.ezza.laporpengaduan.features.lapor;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tbruyelle.rxpermissions2.RxPermissions;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.main.MainActivity;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.utils.DialogUtils;
import io.reactivex.functions.Consumer;

public class LaporActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, LaporActivityComponent {

    private static final int REQ_SETTING_GPS = 99;
    private static final String TAG = LaporActivity.class.getSimpleName();
    private GoogleMap mMap;
    private LinearLayout formContainer;
    private TextInputEditText editKategori;
    private TextInputEditText editKeterangan;
    private LaporActivityPresenter presenter;
    private String[] mPermissions = new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private ProgressBar mProgressLocation;
    private double mLatitude, mLongitude = 0.0;
    private String mImageUri;
    private String mTopikSubs;
    private ProgressDialog pd;
    private Pref pref;

    @Override
    protected void onStart() {
        super.onStart();

        requestPermission();

    }

    private void requestPermission(){
        new RxPermissions(this)
                .request(mPermissions)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Boolean grant) throws Exception {
                        if(grant){
                            onLocationPermissionGrant();
                        }else {
                            Toast.makeText(LaporActivity.this, "Maaf, aplikasi mengharuskan anda untuk mengizinkan akses perangkat lokasi", Toast.LENGTH_SHORT).show();
                            requestPermission();
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapor);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        presenter = new LaporActivityPresenter(this, this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        renderView();
        handleIntent(getIntent());

    }

    private void handleIntent(Intent intent) {
        mImageUri = intent.getStringExtra("image_uri");
    }

    private void onLocationPermissionGrant() {

        getCurrentLocation();

    }

    private void getCurrentLocation() {
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
            return;
        }

        presenter.loadCurrentLocation();
        Log.d(TAG, "getCurrentLocation: load current location");
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Perangkat GPS tidak aktif. Aktifkan?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),REQ_SETTING_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }





    private void renderView() {
        formContainer = (LinearLayout) findViewById(R.id.form_container);
        editKategori = (TextInputEditText) findViewById(R.id.edit_kategori);
        editKategori.setOnClickListener(this);

        mProgressLocation = (ProgressBar)findViewById(R.id.progressBar);

        editKeterangan = (TextInputEditText) findViewById(R.id.edit_keterangan);
        findViewById(R.id.btn_submit_lapor).setOnClickListener(this);
    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_submit_lapor){

            if(validate()){
                submitLapor();
            }


        }else if(view.getId() == R.id.edit_kategori){
            showDialogKategoriItem();
        }
    }

    private void submitLapor() {
        Lapor lapor = new Lapor();
        lapor.setDesc(editKeterangan.getText().toString());
        lapor.setLatitude(mLatitude);
        lapor.setLongitude(mLongitude);
        lapor.setTopikSubscriber(mTopikSubs);
        lapor.setSenderUid(pref.getUser().getUid());
        lapor.setStatus(Lapor.STATUS_ON_PROCESS);

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        presenter.submitLapor(lapor, Uri.parse(mImageUri));

    }

    private boolean validate() {
        EditText[] editTexts = {editKategori, editKeterangan};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }

        if(mLatitude == 0.0){
            DialogUtils.getInstance().showInfoDialog(this, "Error", "Lokasi anda belum di temukan. Mohon tunggu sampai proses pencarian lokasi anda selesai", new DialogUtils.InfoListener() {
                @Override
                public void onDone(DialogInterface dialogInterface, int position) {

                }
            });
            return false;
        }

        return true;
    }

    private void showDialogKategoriItem() {

        final String[] stringArray = getResources().getStringArray(R.array.kategori_nonadmin);

        new AlertDialog.Builder(this)
                .setTitle("Pilih Kategori")
                .setItems(stringArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       editKategori.setText(stringArray[i]);
                        mTopikSubs = stringArray[i].toLowerCase();
                    }
                })
                .create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_SETTING_GPS){
            System.out.println("req from intent back");
//            Toast.makeText(this, "req from intent back setting location", Toast.LENGTH_SHORT).show();
            requestPermission();
        }
    }

    @Override
    public void onLocationResult(Location location) {
        Log.d(TAG, "onLocationResult: "+ location.getLatitude());
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng curLoc = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(curLoc).title("current location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curLoc,14));
        mProgressLocation.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessSubmitLapor(Lapor lapor) {
        presenter.sendNotificationLaporTopik(lapor);
        Log.d(TAG, "onSuccessSubmitLapor: send notification");
        closeProgressDialog();
        DialogUtils.getInstance().showInfoDialog(this, "Berhasil", "Terima kasih telah membantu pemerintah dalam meninggkatkan daerah.\n\nLaporan berhasil di kirim dan mohon untuk menunggu proses verifikasi laporan anda dari pihak terkait", new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                Intent intent = new Intent(LaporActivity.this, MainActivity.class);
                startActivity(intent);
                LaporActivity.this.finish();
            }
        });

    }


    @Override
    public void onError(Exception e) {
        closeProgressDialog();

        DialogUtils.getInstance().showErrorRetryDialog(this, "Terjadi kesalahan saat mengirim laporan ke server", new DialogUtils.ErrorRetryListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {

            }

            @Override
            public void onRetry(DialogInterface dialogInterface, int position) {
                submitLapor();
            }
        });


    }

    @Override
    public void onSuccessSendNotification(String result) {
        Log.d(TAG, "onSuccessSendNotification: "+ result);
    }

    @Override
    public void onErrorSendNotification(Throwable e) {
        e.printStackTrace();
        Log.e(TAG, "onErrorSendNotification: ", e);
    }

    private void closeProgressDialog() {
        if(pd != null)
            pd.dismiss();
    }


}
