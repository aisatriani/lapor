package app.ezza.laporpengaduan.features.admin.main.fragment.daftaraduan;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.adapter.DaftarAduanAdapter;
import app.ezza.laporpengaduan.features.admin.details.DetailsLaporanAdminActivity;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.DialogUtils;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DaftarPengaduanAdminFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DaftarPengaduanAdminFragment extends Fragment implements DaftarPengaduanAdminComponent {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = DaftarPengaduanAdminFragment.class.getSimpleName();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private DaftarPengaduanAdminPresenter presenter;
    private RecyclerView rvDaftarAduan;
    private ProgressBar progressBar;
    private Pref pref;


    public DaftarPengaduanAdminFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DaftarPengaduanAdminFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DaftarPengaduanAdminFragment newInstance(String param1, String param2) {
        DaftarPengaduanAdminFragment fragment = new DaftarPengaduanAdminFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        presenter = new DaftarPengaduanAdminPresenter(getActivity(), this);
        pref = new Pref(getActivity().getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daftar_pengaduan, container, false);
        renderView(view);
        loadDataPengaduan();
        return view;
    }

    private void renderView(View view) {

        rvDaftarAduan = (RecyclerView) view.findViewById(R.id.rv_daftar_aduan);
        rvDaftarAduan.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDaftarAduan.setHasFixedSize(true);
        rvDaftarAduan.setItemViewCacheSize(20);
        rvDaftarAduan.setDrawingCacheEnabled(true);
        rvDaftarAduan.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

    }

    private void loadDataPengaduan() {
        progressBar.setVisibility(View.VISIBLE);

        if(pref.getUser().getType() == User.TYPE_BUPATI || pref.getUser().getType() == User.TYPE_ADMINISTRATOR){
            presenter.loadPengaduanAdminBupati();
        }else {
            presenter.loadMyDataPengaduan();
        }

    }

    @Override
    public void onSuccessMyDataPengaduan(final List<Lapor> laporList) {
        DaftarAduanAdapter adapter = new DaftarAduanAdapter(getActivity(),laporList);
        rvDaftarAduan.setAdapter(adapter);

        adapter.setClickListener(new DaftarAduanAdapter.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(getActivity(), DetailsLaporanAdminActivity.class);
                intent.putExtra(DetailsLaporanAdminActivity.KEY_INTENT_LAPOR, laporList.get(position));
                startActivity(intent);
            }
        });

        if(pref.getUser().getType() == User.TYPE_ADMINISTRATOR)
            adapter.setLongClickListener(new DaftarAduanAdapter.LongClickListener() {
                @Override
                public void onLongClick(View v, final int itemposition) {
                    DialogUtils.getInstance().showYesNoDialog(getActivity(), "Konfirmasi Hapus", "Yakin ingin menghapus data ini??", new DialogUtils.YesNoListener() {
                        @Override
                        public void onYes(DialogInterface dialogInterface, int position) {
                            dialogInterface.dismiss();
                            presenter.deleteLaporan(laporList.get(itemposition));
                        }

                        @Override
                        public void onNo(DialogInterface dialogInterface, int position) {
                            dialogInterface.dismiss();
                        }
                    });

                }
            });

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(DatabaseException e) {
        progressBar.setVisibility(View.GONE);
        DialogUtils.getInstance().showErrorRetryDialog(getActivity(), e.getMessage(), new DialogUtils.ErrorRetryListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {

            }

            @Override
            public void onRetry(DialogInterface dialogInterface, int position) {
                loadDataPengaduan();
            }
        });
    }

    @Override
    public void onDeleteSuccess(Lapor lapor) {
        DialogUtils.getInstance().showInfoDialog(getActivity(), "Berhasil dihapus", "Data laporan berhasil di hapus", new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                dialogInterface.dismiss();
            }
        });
    }
}
