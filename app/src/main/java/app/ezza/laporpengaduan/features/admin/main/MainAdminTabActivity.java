package app.ezza.laporpengaduan.features.admin.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.chat.userchat.UserChatActivity;
import app.ezza.laporpengaduan.features.admin.chat.userchat.UserChatTabActivity;
import app.ezza.laporpengaduan.features.admin.info.InfoAdminActivity;
import app.ezza.laporpengaduan.features.admin.laporan.LaporanAdminActivity;
import app.ezza.laporpengaduan.features.admin.main.fragment.chat.ChatFragment;
import app.ezza.laporpengaduan.features.admin.main.fragment.daftaraduan.DaftarPengaduanAdminFragment;
import app.ezza.laporpengaduan.features.admin.main.fragment.info.InfoAdminFragment;
import app.ezza.laporpengaduan.features.admin.main.fragment.profile.ProfileFragment;
import app.ezza.laporpengaduan.features.admin.pengguna.PenggunaTabActivity;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;

public class MainAdminTabActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {


    private static final String TAG = MainAdminTabActivity.class.getSimpleName();
    public static final String KEY_INTENT_CHAT = "intent.key.chat";
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private FloatingActionButton fab;
    private TabLayout tabLayout;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin_tab);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        setupTabIcon(tabLayout);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        fab.setVisibility(View.GONE);

        mViewPager.addOnPageChangeListener(this);

        handleIntent(getIntent());

    }

    private void handleIntent(Intent intent) {
        if(intent.getExtras() != null){
            if(intent.hasExtra(KEY_INTENT_CHAT)){
                mViewPager.setCurrentItem(2);
            }
        }
    }

    private void setupTabIcon(TabLayout tabLayout) {
        int[] tabIcons = new int[]{
                R.drawable.ic_dashboard_black_24dp,
                R.drawable.ic_notifications_black_24dp,
                R.drawable.ic_message,
                R.drawable.ic_person_white_24dp
        };
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(pref.getUser().getType() == User.TYPE_ADMINISTRATOR || pref.getUser().getType() == User.TYPE_BUPATI)
            getMenuInflater().inflate(R.menu.menu_main_admin_tab, menu);

        if(pref.getUser().getType() == User.TYPE_BUPATI)
            menu.findItem(R.id.action_manage_users).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.action_manage_users){
            Intent intent = new Intent(this, PenggunaTabActivity.class);
            startActivity(intent);
        }
        if(id == R.id.action_report){
            showDialogKategoriReport();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDialogKategoriReport() {
        final String[] items =  getResources().getStringArray(R.array.kategori_nonadmin);
        new AlertDialog.Builder(this)
                .setTitle("Kategori Laporan")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(MainAdminTabActivity.this, LaporanAdminActivity.class);
                        intent.putExtra("kategori",items[i]);
                        startActivity(intent);
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position == 0 || position == 3){
            fab.setVisibility(View.GONE);
            //fab.setImageResource(android.R.drawable.ic_dialog_info);
        }else if(position == 1){
            fab.setVisibility(View.VISIBLE);
            fab.setImageResource(R.drawable.ic_add_alert);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainAdminTabActivity.this, InfoAdminActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
                }
            });
        }
        else if(position == 2){
            fab.setVisibility(View.VISIBLE);
            fab.setImageResource(R.drawable.ic_message);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainAdminTabActivity.this, UserChatTabActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
                }
            });
        }


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main_admin_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if(position == 0){
                return DaftarPengaduanAdminFragment.newInstance(null,null);
            }else if(position == 1){
                return InfoAdminFragment.newInstance(null,null);
            } else if (position == 2){
                return ChatFragment.newInstance(null,null);
            } else if (position  == 3){
                return ProfileFragment.newInstance(null,null);
            }

            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PENGADUAN";
                case 1:
                    return "INFORMASI";
                case 2:
                    return "PESAN";
                case 3:
                    return "PROFIL";
            }
            return null;
        }
    }
}
