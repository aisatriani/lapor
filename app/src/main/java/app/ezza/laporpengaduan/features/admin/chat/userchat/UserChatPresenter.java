package app.ezza.laporpengaduan.features.admin.chat.userchat;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/15/2017.
 */

public class UserChatPresenter {
    private Context context;
    private UserChatComponent viewComponent;
    private Pref pref;

    public UserChatPresenter(Context context, UserChatComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
        this.pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    public void loadUserChat() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userRef = firebaseDatabase.getReference(Pref.REF_USERS);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> users = new ArrayList<User>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    String key = ds.getKey();
                    User user = ds.getValue(User.class);
                    //if(user.getType() != User.TYPE_MEMBER)
                        if(!user.getUid().equals(pref.getUser().getUid()) )
                            users.add(user);
                }
                viewComponent.onSuccessLoadUser(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                viewComponent.onErrorLoadUser(databaseError.toException());
            }
        });

    }

    // type
    // 1 = member
    // 2 = operator
    public void loadUserChat(final int type) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userRef = firebaseDatabase.getReference(Pref.REF_USERS);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> users = new ArrayList<User>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    String key = ds.getKey();
                    User user = ds.getValue(User.class);
                    if(type == 1) {
                        if (user.getType() == User.TYPE_MEMBER)
                            if (!user.getUid().equals(pref.getUser().getUid()))
                                users.add(user);
                    }

                    if(type == 2) {
                        if (user.getType() != User.TYPE_MEMBER)
                            if (!user.getUid().equals(pref.getUser().getUid()))
                                users.add(user);
                    }
                }
                viewComponent.onSuccessLoadUser(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                viewComponent.onErrorLoadUser(databaseError.toException());
            }
        });
    }
}
