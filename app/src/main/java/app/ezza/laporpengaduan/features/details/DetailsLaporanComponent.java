package app.ezza.laporpengaduan.features.details;

import com.google.firebase.database.DatabaseException;

import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/14/2017.
 */

public interface DetailsLaporanComponent {
    void onSuccessLoadDataPengirim(String key, User user);
    void onErrorLoadDataPengirim(DatabaseException e);

    void onSuccessUpdateStatus(Lapor mLapor);

    void onErrorUpdateStatus(Exception e);
}
