package app.ezza.laporpengaduan.features.auth.register;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 6/26/2017.
 */

public class RegisterPresenter {
    private final DatabaseReference databaseReference;
    private Context context;
    private RegisterComponent registerComponent;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private Pref pref;

    public RegisterPresenter(Context context, RegisterComponent registerComponent) {
        this.context = context;
        this.registerComponent = registerComponent;
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");
        pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
    }

    public void submitRegistration(final User users) {
        Task<AuthResult> userWithEmailAndPassword = firebaseAuth.createUserWithEmailAndPassword(users.getEmail(), users.getPassword());
        userWithEmailAndPassword.addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                storeUserToFirebase(authResult, users);
            }
        });
        userWithEmailAndPassword.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                registerComponent.onError(e);
            }
        });
        userWithEmailAndPassword.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                registerComponent.onComplete(task);
            }
        });
    }

    public void storeUserToFirebase(final AuthResult authResult, final User users) {
        FirebaseUser firebaseUser = authResult.getUser();
        users.setUid(firebaseUser.getUid());
        Task<Void> task = databaseReference.child(firebaseUser.getUid()).setValue(users);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                pref.setLogin(true);


                //handle success registration ke view
                registerComponent.onSuccessRegistration(authResult, users);

                //store token auth di preference
                authResult.getUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                    @Override
                    public void onSuccess(GetTokenResult getTokenResult) {
                        pref.storeToken(getTokenResult.getToken());
                    }
                });
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                FirebaseUser userToDelete = authResult.getUser();
                userToDelete.delete();
                registerComponent.onError(e);
            }
        });
    }

    public void updateAkun(final User users) {
        databaseReference.child(users.getUid()).setValue(users).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                registerComponent.onUpdateAkun(users);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                registerComponent.onError(e);
            }
        });
    }
}
