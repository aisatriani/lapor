package app.ezza.laporpengaduan.features.admin.laporandetails;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.Lapor;

/**
 * Created by azisa on 8/19/2017.
 */

public interface LaporanDetailsComponent {
    void onErrorLoadData(DatabaseException e);

    void onSuccessLoadData(List<Lapor> laporList);
}
