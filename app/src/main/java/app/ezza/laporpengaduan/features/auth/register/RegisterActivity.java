package app.ezza.laporpengaduan.features.auth.register;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.iid.FirebaseInstanceId;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.main.MainAdminActivity;
import app.ezza.laporpengaduan.features.auth.login.LoginActivity;
import app.ezza.laporpengaduan.features.ktpverify.KtpVerifyActivity;
import app.ezza.laporpengaduan.features.main.MainActivity;
import app.ezza.laporpengaduan.model.User;

public class RegisterActivity extends AppCompatActivity implements RegisterComponent, View.OnClickListener {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private RegisterPresenter presenter;
    private LinearLayout layoutBottom;
    private TextInputEditText editUsername;
    private TextInputEditText editEmail;
    private TextInputEditText editTelp;
    private TextInputEditText editPassword;
    private TextInputEditText editRepassword;
    private TextView textLoginDisini;
    private ProgressDialog pd;
    private TextInputEditText editNamaLengkap;
    private Pref pref;
    private TextInputEditText editNoKtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
        presenter = new RegisterPresenter(this, this);

        renderView();
    }

    private void renderView() {

        layoutBottom = (LinearLayout) findViewById(R.id.layout_bottom);
        editNoKtp = (TextInputEditText) findViewById(R.id.edit_noktp);
        editNamaLengkap = (TextInputEditText) findViewById(R.id.edit_fullname);
        editUsername = (TextInputEditText) findViewById(R.id.edit_username);
        editEmail = (TextInputEditText) findViewById(R.id.edit_email);
        editTelp = (TextInputEditText) findViewById(R.id.edit_telp);
        editPassword = (TextInputEditText) findViewById(R.id.edit_password);
        editRepassword = (TextInputEditText) findViewById(R.id.edit_repassword);
        findViewById(R.id.btn_daftar).setOnClickListener(this);
        textLoginDisini = (TextView) findViewById(R.id.text_login_disini);
        textLoginDisini.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_daftar){
            if(validate()){
                submitRegistration();
            }
        }else if (view.getId() == R.id.text_login_disini){
            onBackPressed();
        }
    }

    private void submitRegistration() {

        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        User users = new User();
        users.setNoktp(editNoKtp.getText().toString());
        users.setNama(editNamaLengkap.getText().toString());
        users.setUsername(editUsername.getText().toString());
        users.setEmail(editEmail.getText().toString());
        users.setTelp(editTelp.getText().toString());
        users.setPassword(editPassword.getText().toString());
        users.setType(User.TYPE_MEMBER);

        presenter.submitRegistration(users);

    }

    private boolean validate() {
        EditText[] editTexts = {editNoKtp, editNamaLengkap, editUsername, editEmail, editTelp, editPassword, editRepassword};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }


        String email = editEmail.getText().toString();
        if(!email.matches(Patterns.EMAIL_ADDRESS.pattern())){
            editEmail.setError("format email tidak benar");
            editEmail.requestFocus();
            return false;
        }


        return true;
    }

    @Override
    public void onSuccessRegistration(AuthResult authResult, final User users) {

        String tokenFrm = FirebaseInstanceId.getInstance().getToken();
        //set token firebase messaging for notification
        users.setTokenFcm(tokenFrm);
        pref.storeUser(users);

        Log.d(TAG, "onSuccessRegistration: registration complete user uid :" + users.getUid());

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Registration success")
                .setMessage("Selamat, anda telah berhasil mendaftar ke aplikasi ini.")

                .setPositiveButton("Ke aplikasi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(users.isKtpVerify()){
                            if(users.getType()== User.TYPE_MEMBER){
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(intent);
                                RegisterActivity.this.finish();
                            }else {
                                Intent intent = new Intent(RegisterActivity.this, MainAdminActivity.class);
                                startActivity(intent);
                                RegisterActivity.this.finish();
                            }
                        }else {
                            Intent intent = new Intent(RegisterActivity.this, KtpVerifyActivity.class);
                            intent.putExtra(KtpVerifyActivity.INTENT_STATUS, KtpVerifyActivity.INT_KTP_VERIFY);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }
                })
                .create().show();

    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();

        if(pd != null)
            pd.dismiss();

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Error")
                .setMessage(e.getMessage())
                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        submitRegistration();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();

    }

    @Override
    public void onComplete(Task<AuthResult> task) {

        if(pd != null)
            pd.dismiss();

    }

    @Override
    public void onUpdateAkun(User users) {

    }
}
