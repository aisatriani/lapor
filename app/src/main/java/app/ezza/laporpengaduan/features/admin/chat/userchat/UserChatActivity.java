package app.ezza.laporpengaduan.features.admin.chat.userchat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.adapter.UserChatAdapter;
import app.ezza.laporpengaduan.features.admin.chat.chatdata.ChatDataActivity;
import app.ezza.laporpengaduan.model.User;

public class UserChatActivity extends AppCompatActivity implements UserChatComponent {

    private RecyclerView rvUser;
    private UserChatPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat);

        presenter = new UserChatPresenter(this, this);

        renderView();
    }

    private void renderView() {

        rvUser = (RecyclerView) findViewById(R.id.rv_userchat);
        rvUser.setLayoutManager(new LinearLayoutManager(this));

        presenter.loadUserChat();

    }

    @Override
    public void onSuccessLoadUser(final List<User> users) {
        UserChatAdapter adapter = new UserChatAdapter(this,users);
        rvUser.setAdapter(adapter);
        adapter.setClickListener(new UserChatAdapter.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(UserChatActivity.this, ChatDataActivity.class);
                intent.putExtra(ChatDataActivity.KEY_INTENT_USER, users.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onErrorLoadUser(DatabaseException e) {

    }
}
