package app.ezza.laporpengaduan.features.admin.chat.userchat;

import com.google.firebase.database.DatabaseException;

import java.util.List;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/15/2017.
 */

public interface UserChatComponent {
    void onSuccessLoadUser(List<User> users);

    void onErrorLoadUser(DatabaseException e);
}
