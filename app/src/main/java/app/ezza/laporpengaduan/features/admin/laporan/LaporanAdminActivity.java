package app.ezza.laporpengaduan.features.admin.laporan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DatabaseException;

import java.util.ArrayList;
import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.laporandetails.LaporanAdminDetailsActivity;
import app.ezza.laporpengaduan.model.Lapor;

public class LaporanAdminActivity extends AppCompatActivity implements LaporanAdminComponent, View.OnClickListener {

    private String mKategori;
    private PieChart chart;
    private Button btnLaporanEksekusi;
    private Button btnLaporanTolak;
    private LaporanAdminPresenter presenter;
    private ProgressDialog pd;
    private long mCountEksekusi;
    private long mCountTolak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_admin);

        presenter = new LaporanAdminPresenter(this,this);

        mKategori = getIntent().getStringExtra("kategori");
        setTitle(mKategori);

        btnLaporanEksekusi = (Button) findViewById(R.id.btn_laporan_eksekusi);
        btnLaporanTolak = (Button) findViewById(R.id.btn_laporan_tolak);
        btnLaporanEksekusi.setOnClickListener(this);
        btnLaporanTolak.setOnClickListener(this);
        chart = (PieChart) findViewById(R.id.chart);

        loadChartData();

    }



    private void loadChartData() {
        pd = new ProgressDialog(this);
        pd.setMessage("Memuat data..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        presenter.loadChartData(mKategori.toLowerCase());
    }

    @Override
    public void onResultCountDataChart(long countEksekusi, long countTolak) {

        mCountEksekusi = countEksekusi;
        mCountTolak = countTolak;
        pd.dismiss();

        List<PieEntry> entries = new ArrayList<>();
        entries.add( new PieEntry(countEksekusi,"Eksekusi"));
        entries.add( new PieEntry(countTolak,"Tolak"));

        PieDataSet pieDataSet = new PieDataSet(entries,"Chart");
        pieDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        PieData pieData = new PieData(pieDataSet);

        chart.setData(pieData);
        Description desc = new Description();
        desc.setText("Statistik Laporan");
        chart.setDescription(desc);
        chart.setEntryLabelColor(Color.WHITE);
        chart.setNoDataText("Data Kosong");
        chart.setNoDataTextColor(Color.RED);
        chart.invalidate();
    }

    @Override
    public void onErrorCountDataChart(DatabaseException e) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_laporan_eksekusi){

            if(mCountEksekusi == 0){
                Toast.makeText(this, "Tidak ada data yang di eksekusi", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent intent = new Intent(this, LaporanAdminDetailsActivity.class);
            intent.putExtra("kategori",mKategori);
            intent.putExtra("status", Lapor.STATUS_ON_SUCCESS);
            startActivity(intent);

        }else if(view.getId() == R.id.btn_laporan_tolak){

            if(mCountTolak == 0){
                Toast.makeText(this, "Tidak ada data yang di tolak", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent intent = new Intent(this, LaporanAdminDetailsActivity.class);
            intent.putExtra("kategori",mKategori);
            intent.putExtra("status",Lapor.STATUS_ON_CANCEL);
            startActivity(intent);
        }
    }
}
