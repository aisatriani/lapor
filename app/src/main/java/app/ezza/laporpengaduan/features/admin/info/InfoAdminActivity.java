package app.ezza.laporpengaduan.features.admin.info;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.utils.DialogUtils;

public class InfoAdminActivity extends AppCompatActivity implements View.OnClickListener, InfoAdminComponent {


    private TextInputEditText editContentInfo;
    private TextInputEditText editTitleInfo;
    private InfoAdminPresenter presenter;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_admin);

        presenter = new InfoAdminPresenter(this,this);

        renderView();
    }

    private void renderView() {
        editTitleInfo = (TextInputEditText) findViewById(R.id.edit_title_info);
        editContentInfo = (TextInputEditText) findViewById(R.id.edit_content_info);
        findViewById(R.id.btn_submit_info).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_submit_info){
            if(validate()){
                submitInfo();

            }
        }
    }

    private void submitInfo() {

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        presenter.submitInfo(editTitleInfo.getText().toString(), editContentInfo.getText().toString());
    }

    private boolean validate() {
        EditText[] editTexts = {editTitleInfo, editContentInfo};
        for (EditText editText : editTexts) {
            if (editText.getText().toString().trim().equalsIgnoreCase("")) {
                editText.setError("tidak boleh kosong");
                editText.requestFocus();
                return false;
            }
        }

        return true;
    }

    @Override
    public void onSuccessSubmit() {
        DialogUtils.getInstance().showInfoDialog(this, "Berhasil", "Info berhasil dibuat", new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                Toast.makeText(InfoAdminActivity.this, "Berhasil di buat", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
        DialogUtils.getInstance().showErrorRetryDialog(this, e.getMessage(), new DialogUtils.ErrorRetryListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {

            }

            @Override
            public void onRetry(DialogInterface dialogInterface, int position) {
                submitInfo();
            }
        });

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onProgressVisibility(int visibility) {
        if(visibility == View.GONE){
            if(pd != null)
                pd.dismiss();
        }
    }
}
