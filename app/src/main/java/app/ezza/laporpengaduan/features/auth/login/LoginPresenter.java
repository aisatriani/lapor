package app.ezza.laporpengaduan.features.auth.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 6/26/2017.
 */

public class LoginPresenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();
    private final FirebaseDatabase firebaseDatabase;
    private final DatabaseReference usersRef;
    private LoginComponent loginComponent;
    private FirebaseAuth firebaseAuth;

    public LoginPresenter(LoginComponent loginComponent) {
        this.loginComponent = loginComponent;
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        usersRef = firebaseDatabase.getReference("users");
    }

    public void login(String email, String password) {

        Task<AuthResult> authResultTask = firebaseAuth.signInWithEmailAndPassword(email, password);
        authResultTask.addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(final AuthResult authResult) {

                authResult.getUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                    @Override
                    public void onSuccess(GetTokenResult getTokenResult) {
                        loginComponent.onTokenReceived(getTokenResult.getToken());

                        usersRef.child(authResult.getUser().getUid()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                User value = dataSnapshot.getValue(User.class);
                                String tokenFrm = FirebaseInstanceId.getInstance().getToken();
                                //set token firebase messaging for notification
                                value.setTokenFcm(tokenFrm);

                                loginComponent.onSuccessLogin(authResult, value);
                                Log.d(TAG, "onDataChange: on success login");
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                databaseError.toException().printStackTrace();
                                //loginComponent.onError(databaseError.toException());
                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loginComponent.onError(e);
                    }
                }).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    @Override
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        loginComponent.onCompleted();
                    }
                });


            }
        });
        authResultTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                loginComponent.onError(e);
            }
        });
        authResultTask.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //loginComponent.onCompleted();
            }
        });
    }
}
