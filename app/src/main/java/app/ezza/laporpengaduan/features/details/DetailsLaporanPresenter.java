package app.ezza.laporpengaduan.features.details;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 7/14/2017.
 */

public class DetailsLaporanPresenter {

    private Context context;
    private DetailsLaporanComponent viewComponent;

    public DetailsLaporanPresenter(Context context, DetailsLaporanComponent viewComponent) {
        this.context = context;
        this.viewComponent = viewComponent;
    }

    public void loadDataPengirim(String uid) {

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.getReference(Pref.REF_USERS).child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                User user = dataSnapshot.getValue(User.class);
                viewComponent.onSuccessLoadDataPengirim(key,user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewComponent.onErrorLoadDataPengirim(databaseError.toException());
            }
        });

    }

    public void updateStatusLaporan(final Lapor mLapor, final String status) {
        mLapor.setStatus(status);
        DatabaseReference laporRef = FirebaseDatabase.getInstance().getReference(Pref.REF_LAPOR);
        laporRef.child(mLapor.getUid()).setValue(mLapor).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                viewComponent.onSuccessUpdateStatus(mLapor);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onErrorUpdateStatus(e);
            }
        });

    }
}
