package app.ezza.laporpengaduan.features.admin.info;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Info;

/**
 * Created by azisa on 6/28/2017.
 */

public class InfoAdminPresenter {
    private final DatabaseReference infoReference;

    private InfoAdminComponent viewComponent;
    private Context context;
    private Pref pref;


    public InfoAdminPresenter(InfoAdminComponent viewComponent, Context context) {
        this.viewComponent = viewComponent;
        this.context = context;

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        infoReference = firebaseDatabase.getReference("info");
        pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME,Context.MODE_PRIVATE));
    }

    public void submitInfo(String title, String content) {
        Info info = new Info();
        info.setTitle(title);
        info.setContent(content);
        info.setSenderUid(pref.getUser().getUid());

        Task<Void> task = infoReference.push().setValue(info);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                viewComponent.onSuccessSubmit();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                viewComponent.onError(e);
                viewComponent.onProgressVisibility(View.GONE);
            }
        });
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                viewComponent.onComplete();
                viewComponent.onProgressVisibility(View.GONE);
            }
        });

    }
}
