package app.ezza.laporpengaduan.features.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.concurrent.atomic.AtomicMarkableReference;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.chat.userchat.UserChatActivity;
import app.ezza.laporpengaduan.features.admin.chat.userchat.UserChatTabActivity;
import app.ezza.laporpengaduan.features.admin.main.MainAdminActivity;
import app.ezza.laporpengaduan.features.admin.main.MainAdminTabActivity;
import app.ezza.laporpengaduan.features.admin.main.fragment.chat.ChatFragment;
import app.ezza.laporpengaduan.features.admin.main.fragment.profile.ProfileFragment;
import app.ezza.laporpengaduan.features.auth.login.LoginActivity;
import app.ezza.laporpengaduan.features.main.fragment.daftaraduan.DaftarPengaduanFragment;
import app.ezza.laporpengaduan.features.main.fragment.info.InfoFragment;
import app.ezza.laporpengaduan.features.main.fragment.lapor.LaporFragment;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_info:
                    changeContent(InfoFragment.newInstance("test","test"));
                    menu.findItem(R.id.action_add_chat).setVisible(false);
                    return true;
                case R.id.navigation_lapor:
                    changeContent(LaporFragment.newInstance(null,null));
                    menu.findItem(R.id.action_add_chat).setVisible(false);
                    return true;
                case R.id.navigation_daftar_pengaduan:
                    changeContent(DaftarPengaduanFragment.newInstance(null,null));
                    menu.findItem(R.id.action_add_chat).setVisible(false);
                    return true;
                case R.id.navigation_chat:
                    changeContent(ChatFragment.newInstance(null,null));
                    menu.findItem(R.id.action_add_chat).setVisible(true);
                    return true;
                case R.id.navigation_profile:
                    changeContent(ProfileFragment.newInstance(null,null));
                    menu.findItem(R.id.action_add_chat).setVisible(false);
                    return true;
            }
            return false;
        }

    };

    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.setProperty("java.net.preferIPv4Stack" , "true");

        String token = FirebaseInstanceId.getInstance().getToken();
        if(token != null && mAuth.getCurrentUser() != null)
            updateRegistrationToken(token);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        changeContent(DaftarPengaduanFragment.newInstance(null,null));
    }

    private void updateRegistrationToken(String refreshedToken) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        DatabaseReference reference = firebaseDatabase.getReference(Pref.REF_USERS);
        reference.child(currentUser.getUid()).child(Pref.TOKEN_FIELD).setValue(refreshedToken).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("success update token");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                System.out.println("error update token to server");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        this.menu = menu;
        menu.findItem(R.id.action_add_chat).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_admin){
            Intent intent = new Intent(this, MainAdminTabActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.action_add_chat){
            Intent intent = new Intent(this, UserChatTabActivity.class);
            startActivityForResult(intent, 200);
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeContent(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

}
