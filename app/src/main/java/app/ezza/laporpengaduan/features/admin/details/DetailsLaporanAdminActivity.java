package app.ezza.laporpengaduan.features.admin.details;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseException;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.DialogUtils;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class DetailsLaporanAdminActivity extends AppCompatActivity implements OnMapReadyCallback, DetailsLaporanAdminComponent, View.OnClickListener {

    public static final String KEY_INTENT_LAPOR = "lapor";
    private static final String TAG = DetailsLaporanAdminActivity.class.getSimpleName();
    private Lapor mLapor;
    private GoogleMap mMap;
    private LatLng mLatLng;
    private ImageView imgLapor;
    private TextView textDesc;
    private DetailsLaporanAdminPresenter presenter;
    private Button btnTerima;
    private Button btnEksekusi;
    private Button btnTolak;
    private TextView textNamaPengirim;
    private TextView textEmailPengirim;
    private Pref pref;
    private ProgressDialog progressDialog;
    private TextView textStatus;
    private Context context;
    private LinearLayout layoutButtonAction;
    private Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_laporan_admin);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        presenter = new DetailsLaporanAdminPresenter(this, this);

        handleIntent(getIntent());

        renderView();

        presenter.loadDataPengirim(mLapor.getSenderUid());

    }

    private void handleIntent(Intent intent) {
        mLapor = (Lapor)intent.getSerializableExtra(KEY_INTENT_LAPOR);
        mLatLng = new LatLng(mLapor.getLatitude(), mLapor.getLongitude());
    }

    private void renderView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        imgLapor = (ImageView) findViewById(R.id.img_lapor);
        textDesc = (TextView) findViewById(R.id.text_desc_detail);

        textNamaPengirim = (TextView) findViewById(R.id.text_nama_lengkap);
        textEmailPengirim = (TextView) findViewById(R.id.text_email);
        textStatus = (TextView) findViewById(R.id.text_status_detail);

        btnTerima = (Button) findViewById(R.id.btn_terima);
        btnTerima.setOnClickListener(this);
        btnEksekusi = (Button) findViewById(R.id.btn_eksekusi);
        btnEksekusi.setOnClickListener(this);
        btnTolak = (Button) findViewById(R.id.btn_tolak);
        btnTolak.setOnClickListener(this);

        layoutButtonAction = (LinearLayout) findViewById(R.id.layout_button_action);

        Transformation transformation = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = imgLapor.getWidth();

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };

        Picasso.with(this)
                .load(mLapor.getImgUrl())
                .transform(transformation)
                .into(imgLapor);

        textDesc.setText(mLapor.getDesc());

        initView();

    }

    private void initView() {
        setStatusLaporan(mLapor);
        if(pref.getUser().getType() ==  User.TYPE_ADMINISTRATOR || pref.getUser().getType() == User.TYPE_BUPATI){
            layoutButtonAction.setVisibility(View.GONE);
        }
    }

    private void setStatusLaporan(Lapor mLapor) {
        if(mLapor.getStatus().equals(Lapor.STATUS_ON_PROCESS)){
            textStatus.setText(getString(R.string.laporan_proses));
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_ACCEPT)){
            textStatus.setText(getString(R.string.laporan_terima));
            btnTerima.setVisibility(View.GONE);
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_SUCCESS)){
            textStatus.setText(getString(R.string.laporan_eksekusi));
            btnTerima.setVisibility(View.GONE);
            btnEksekusi.setVisibility(View.GONE);
            btnTolak.setVisibility(View.GONE);
        }else if(mLapor.getStatus().equals(Lapor.STATUS_ON_CANCEL)){
            textStatus.setText(getString(R.string.laporan_tolak));
            btnTerima.setVisibility(View.GONE);
            btnEksekusi.setVisibility(View.GONE);
            btnTolak.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
        mMap.addMarker(new MarkerOptions().position(mLatLng));
    }

    @Override
    public void onSuccessLoadDataPengirim(String key, User user) {
        textNamaPengirim.setText(user.getNama());
        textEmailPengirim.setText(user.getEmail());
    }

    @Override
    public void onErrorLoadDataPengirim(DatabaseException e) {
        closeProgressLoading();

        if(context != null && !DetailsLaporanAdminActivity.this.isFinishing())
            DialogUtils.getInstance().showErrorRetryDialog(context, e.getMessage(), new DialogUtils.ErrorRetryListener() {
                @Override
                public void onDone(DialogInterface dialogInterface, int position) {
                    dialogInterface.dismiss();
                }

                @Override
                public void onRetry(DialogInterface dialogInterface, int position) {
                    presenter.loadDataPengirim(mLapor.getSenderUid());
                }
            });
    }

    @Override
    public void onSuccessUpdateStatus(Lapor lapor) {
        closeProgressLoading();
        DialogUtils.getInstance().showInfoDialog(this, "Informasi", "Laporan berhasil di " + lapor.getStatus(), new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {
                dialogInterface.dismiss();
            }
        });

        setStatusLaporan(lapor);

    }

    @Override
    public void onErrorUpdateStatus(Exception e) {
        closeProgressLoading();
        DialogUtils.getInstance().showInfoDialog(this, "Error", e.getMessage(), new DialogUtils.InfoListener() {
            @Override
            public void onDone(DialogInterface dialogInterface, int position) {

            }
        });
    }

    @Override
    public void onSuccessSendNotification(String s) {

    }

    @Override
    public void onErrorSendNotification(Throwable e) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_terima){
            DialogUtils.getInstance().showYesNoDialog(this, "Konfirmasi status", "Perubahan status tdk dapat di edit kembali. Yakin ingin merubah status laporan ini?", new DialogUtils.YesNoListener() {
                @Override
                public void onYes(DialogInterface dialogInterface, int position) {
                    doActionTerima();
                }

                @Override
                public void onNo(DialogInterface dialogInterface, int position) {

                }
            });

        }else if(view.getId() == R.id.btn_eksekusi){

            View viewDialog = getLayoutInflater().inflate(R.layout.layout_dialog_eksekusi, null);

            final AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setView(viewDialog)
                    .create();

            Button btnDialogEksekusi = (Button) viewDialog.findViewById(R.id.btn_dialog_eksekusi);
            final EditText editKeterangan = (EditText)viewDialog.findViewById(R.id.edit_keterangan_dialog);
            final ImageView imagePickDialog = (ImageView)viewDialog.findViewById(R.id.img_pick_dialog);

            imagePickDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RxImagePicker.with(DetailsLaporanAdminActivity.this)
                            .requestImage(Sources.CAMERA)
                            .subscribe(new Consumer<Uri>() {
                                @Override
                                public void accept(@NonNull Uri uri) throws Exception {
                                    Picasso.with(context)
                                            .load(uri)
                                            .into(imagePickDialog);

                                    imgUri = uri;
                                }
                            });
                }
            });

            btnDialogEksekusi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(imgUri == null){
                        editKeterangan.setError("Harus di sertakan dengan foto");
                        editKeterangan.requestFocus();
                        return;
                    }

                    if(editKeterangan.getText().toString().equals("")){
                        editKeterangan.setError("keterangan harus di isi");
                        editKeterangan.requestFocus();
                        return;
                    }

                    doActionEksekusi(imgUri, editKeterangan.getText().toString(), mLapor, Lapor.STATUS_ON_SUCCESS);

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();

        }else if(view.getId() == R.id.btn_tolak){

            View viewDialog = getLayoutInflater().inflate(R.layout.layout_dialog_tolak, null);

            final AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setView(viewDialog)
                    .create();

            Button btnDialogTolak = (Button) viewDialog.findViewById(R.id.btn_dialog_tolak);
            final EditText editKeterangan = (EditText)viewDialog.findViewById(R.id.edit_keterangan_tolak);


            btnDialogTolak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(editKeterangan.getText().toString().equals("")){
                        editKeterangan.setError("keterangan harus di isi");
                        editKeterangan.requestFocus();
                        return;
                    }

                    doActionTolak(editKeterangan.getText().toString(), mLapor, Lapor.STATUS_ON_SUCCESS);

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
        }
    }

    private void doActionTolak(String keterangan, Lapor mLapor, String statusOnSuccess) {
        showProgressLoading();
        presenter.tolakLaporan(keterangan, this.mLapor, Lapor.STATUS_ON_CANCEL);
    }

    private void doActionEksekusi(Uri imgUri, String keterangan, Lapor mLapor, String statusOnSuccess) {
        showProgressLoading();
        //presenter.updateStatusLaporan(mLapor, Lapor.STATUS_ON_SUCCESS);
        presenter.eksekusiLaporan(imgUri,keterangan, mLapor, statusOnSuccess);
    }

    private void doActionTerima() {
        showProgressLoading();
        presenter.updateStatusLaporan(mLapor, Lapor.STATUS_ON_ACCEPT);
    }

    private void showProgressLoading(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void closeProgressLoading(){
        if(progressDialog != null)
            progressDialog.dismiss();
    }
}
