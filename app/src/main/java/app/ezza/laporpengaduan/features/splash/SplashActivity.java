package app.ezza.laporpengaduan.features.splash;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.features.admin.main.MainAdminActivity;
import app.ezza.laporpengaduan.features.admin.main.MainAdminTabActivity;
import app.ezza.laporpengaduan.features.auth.login.LoginActivity;
import app.ezza.laporpengaduan.features.auth.register.RegisterActivity;
import app.ezza.laporpengaduan.features.ktpverify.KtpVerifyActivity;
import app.ezza.laporpengaduan.features.main.MainActivity;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.DialogUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.schedulers.RxThreadFactory;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends AppCompatActivity implements ValueEventListener {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final long TIME_INTERVAL = 3;

    private FirebaseAuth mAuth;

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseDatabase.getInstance().goOnline();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mAuth = FirebaseAuth.getInstance();

        final Observable observable = Observable.interval(TIME_INTERVAL, TimeUnit.SECONDS);
        observable
                .take(1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        checkIfLogin();
                        observable.unsubscribeOn(Schedulers.newThread());
                    }
                });


    }

    private DatabaseReference userDbRef;

    private ValueEventListener connectedValueListener = new ValueEventListener() {
        @Override
        public void onDataChange(final DataSnapshot dataSnapshot) {
            boolean connected = dataSnapshot.getValue(Boolean.class);
            if(!connected){

                DialogUtils.getInstance().showErrorRetryDialog(SplashActivity.this, "Gagal terhubung ke jaringan. Periksa koneksi internet anda", new DialogUtils.ErrorRetryListener() {
                    @Override
                    public void onDone(DialogInterface dialogInterface, int position) {
                        dataSnapshot.getRef().removeEventListener(connectedValueListener);
                        SplashActivity.this.finish();
                    }

                    @Override
                    public void onRetry(DialogInterface dialogInterface, int position) {
                        dataSnapshot.getRef().removeEventListener(connectedValueListener);
                        Toast.makeText(SplashActivity.this, "retry connection to server", Toast.LENGTH_SHORT).show();
                        checkIfLogin();
                    }
                });
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@android.support.annotation.NonNull final FirebaseAuth firebaseAuth) {
            mAuth.removeAuthStateListener(authStateListener);
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();

            if(currentUser != null){

                Log.d(TAG, "onAuthStateChanged: is already login "+ currentUser.getEmail());
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                userDbRef = database.getReference(Pref.REF_USERS).child(currentUser.getUid());
                userDbRef.addValueEventListener(SplashActivity.this);

                //database.getReference(".info/connected").addValueEventListener(connectedValueListener);

                return;
            }{
                System.out.println("not login in");
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }
    };

    private void checkIfLogin() {
        System.out.println("cek login 1 kali");
        mAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        if(userDbRef != null)
            userDbRef.removeEventListener(this);

        mAuth.removeAuthStateListener(authStateListener);
        FirebaseDatabase.getInstance().getReference(".info/connected").removeEventListener(connectedValueListener);
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        if(userDbRef != null)
            userDbRef.removeEventListener(this);

        mAuth.removeAuthStateListener(authStateListener);
        FirebaseDatabase.getInstance().getReference(".info/connected").removeEventListener(connectedValueListener);
        super.onDestroy();
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        User user = dataSnapshot.getValue(User.class);
        assert user != null;
        if(user.isKtpCancel()){
            Intent intent = new Intent(SplashActivity.this, KtpVerifyActivity.class);
            intent.putExtra(KtpVerifyActivity.INTENT_STATUS, KtpVerifyActivity.INT_KTP_CANCEL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        if(user.isKtpVerify()){
            if(user.getType() == User.TYPE_MEMBER){
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();

            }else {
                Intent intent = new Intent(SplashActivity.this, MainAdminTabActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }else {
            Intent intent = new Intent(SplashActivity.this, KtpVerifyActivity.class);
            intent.putExtra(KtpVerifyActivity.INTENT_STATUS, KtpVerifyActivity.INT_KTP_VERIFY);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }


    }



    @Override
    public void onCancelled(DatabaseError databaseError) {
        System.out.println(databaseError.getMessage());
//                        DialogUtils.getInstance().showErrorRetryDialog(SplashActivity.this, databaseError.getMessage(), new DialogUtils.ErrorRetryListener() {
//                            @Override
//                            public void onDone(DialogInterface dialogInterface, int position) {
//
//                            }
//
//                            @Override
//                            public void onRetry(DialogInterface dialogInterface, int position) {
//
//                            }
//                        });
    }

}
