package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Info;
import app.ezza.laporpengaduan.utils.TextUtils;

public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.ViewHolder> {

    private List<Info> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textTitleInfo;
        private final TextView textContenteInfo;
        private final TextView textDateInfo;
        View v;


        ViewHolder(View v) {

            super(v);
            this.v = v;

            textTitleInfo = (TextView) v.findViewById(R.id.text_title_info);
            textContenteInfo = (TextView) v.findViewById(R.id.text_contente_info);
            textDateInfo = (TextView) v.findViewById(R.id.text_date_info);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public InfoAdapter(Context context, List<Info> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.textTitleInfo.setText(list.get(position).getTitle());
        holder.textContenteInfo.setText(list.get(position).getContent());
        holder.textDateInfo.setText(TextUtils.timeMilisToText(list.get(position).getCreated_at()));

        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addItem(Info info){
        list.add(info);
        this.notifyDataSetChanged();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}