package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Lapor;
import app.ezza.laporpengaduan.utils.TextUtils;

public class DaftarAduanAdapter extends RecyclerView.Adapter<DaftarAduanAdapter.ViewHolder> {

    private List<Lapor> list;
    private ClickListener clickListener;
    private LongClickListener longClickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgIcon;
        private final TextView textDesc;
        private final TextView textTitle;
        private final TextView textDateLapor;
        // each data item is just a string in this case

        //TextView tv_title;
        View v;


        ViewHolder(View v) {

            super(v);
            this.v = v;

            imgIcon = (ImageView) v.findViewById(R.id.img_icon);
            textDesc = (TextView) v.findViewById(R.id.text_desc);
            textTitle = (TextView) v.findViewById(R.id.text_title_lapor);
            textDateLapor = (TextView) v.findViewById(R.id.text_date_lapor);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DaftarAduanAdapter(Context context, List<Lapor> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftar_aduan, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Picasso.with(context)
                .load(list.get(position).getImgUrl())
                .fit()
                .centerCrop()
                .into(holder.imgIcon);

        holder.textDesc.setText(list.get(position).getDesc());
        holder.textTitle.setText(list.get(position).getTopikSubscriber());
        holder.textDateLapor.setText(TextUtils.timeMilisToText(list.get(position).getCreated_at()));

        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(longClickListener != null)
                    longClickListener.onLongClick(view,position);
                return false;
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public interface LongClickListener {
        void onLongClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

    public void setLongClickListener(LongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }
}