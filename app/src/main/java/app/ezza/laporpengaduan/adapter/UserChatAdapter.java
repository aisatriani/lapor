package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Chat;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.CircleTransform;

public class UserChatAdapter extends RecyclerView.Adapter<UserChatAdapter.ViewHolder> {

    private List<User> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgUser;
        private final TextView textFullname;
        private final TextView textUnitKerja;
        // each data item is just a string in this case

        //TextView tv_title;
        View v;


        ViewHolder(View v) {

            super(v);
            this.v = v;

            //tv_title = (TextView)v.findViewById(R.id.tv_title);
            imgUser = (ImageView) v.findViewById(R.id.img_user);
            textFullname = (TextView) v.findViewById(R.id.text_fullname);
            textUnitKerja = (TextView) v.findViewById(R.id.text_unitkerja);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public UserChatAdapter(Context context, List<User> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_userchat, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.tv_title.setText(list.get(position).getNama_tour());
        User user = list.get(position);
        holder.textFullname.setText(user.getNama());
        holder.textUnitKerja.setText(Topik.UserTypeToTopik(user.getType()));

        Picasso.with(context)
                .load(user.getImageUrl())
                .placeholder(R.drawable.ic_account_circle)
                .transform(new CircleTransform())
                .into(holder.imgUser);

        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}