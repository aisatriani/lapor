package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.Chat;
import app.ezza.laporpengaduan.utils.TextUtils;
import me.himanshusoni.chatmessageview.ChatMessageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private static final int VIEW_MINE = 1;
    private static final int VIEW_OTHER = 2;
    private List<Chat> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ChatMessageView chatMessageView;
        private final TextView textMessage;
        private final TextView tvTime;
        // each data item is just a string in this case

        //TextView tv_title;
        View v;


        ViewHolder(View v) {

            super(v);
            this.v = v;

            //tv_title = (TextView)v.findViewById(R.id.tv_title);
            chatMessageView = (ChatMessageView) v.findViewById(R.id.chatMessageView);
            textMessage = (TextView) v.findViewById(R.id.tv_message);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatAdapter(Context context, List<Chat> myDataset) {
        this.context = context;
        list = myDataset;
    }

    @Override
    public int getItemViewType(int position) {
        Chat chat = list.get(position);
        Pref pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        if(chat.senderUid.equals(pref.getUser().getUid()))
            return VIEW_MINE;
        return VIEW_OTHER;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        if(viewType == VIEW_MINE){
             v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_mine, parent, false);
        }else{
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_other, parent, false);
        }

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.tv_title.setText(list.get(position).getNama_tour());

        holder.textMessage.setText(list.get(position).message);
        String time = TextUtils.getFormattedDate(context, list.get(position).timestamp);
        holder.tvTime.setText(time);


        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    public void addMessage(Chat chat){
        list.add(chat);
        notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}