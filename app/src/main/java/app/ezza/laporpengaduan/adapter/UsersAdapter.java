package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.User;
import app.ezza.laporpengaduan.utils.FcmUtils;
import app.ezza.laporpengaduan.utils.MailService;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private List<User> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout itemContainer;
        private final ImageView imgUser;
        private final TextView textFullname;
        private final TextView textNoktp;
        private final Button btnVerify;
        private final Button btnTolak;

        View v;

        ViewHolder(View v) {

            super(v);
            this.v = v;

            //tv_title = (TextView)v.findViewById(R.id.tv_title);
            itemContainer = (LinearLayout) v.findViewById(R.id.item_container);
            imgUser = (ImageView) v.findViewById(R.id.img_user);
            textFullname = (TextView) v.findViewById(R.id.text_fullname);
            textNoktp = (TextView) v.findViewById(R.id.text_noktp);
            btnVerify = (Button) v.findViewById(R.id.btn_verify);
            btnTolak = (Button)v.findViewById(R.id.btn_tolak);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public UsersAdapter(Context context, List<User> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.tv_title.setText(list.get(position).getNama_tour());

        final User user = list.get(position);
        holder.textFullname.setText(user.getNama());
        holder.textNoktp.setText(user.getNoktp());

        Picasso.with(context)
                .load(user.getImageUrl())
                .placeholder(R.drawable.ic_account_circle)
                .into(holder.imgUser);
        
        holder.btnVerify.setVisibility(View.VISIBLE);

        if(user.isKtpVerify())
            holder.btnVerify.setVisibility(View.GONE);

        holder.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View view) {
                String text = "Selamat akun anda telah aktiv. Silahkan buka aplikasi Laporan pengaduan di smartphone anda. \n\nTerima kasih telah berpartisipasi dalam membangun daerah khususnya di kabupaten gorontalo.";
                final MailService mailService = new MailService("pengaduankabgor@gmail.com",user.getEmail(),"KONFIRMASI AKUN",text,text);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mailService.sendAuthenticated();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();


                FirebaseDatabase instance = FirebaseDatabase.getInstance();
                user.setKtpVerify(true);
                user.setKtpCancel(false);
                instance.getReference(Pref.REF_USERS).child(user.getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Pengguna berhasil di verifikasi", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        holder.btnTolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = "Pendaftaran anda di tolak karena data anda tidak sesuai. Silahkan hub operator di kantor pelayanan wajib untuk memverifikasi langsung data anda. \n\nTerima kasih telah berpartisipasi dalam membangun daerah khususnya di kabupaten gorontalo.";
                final MailService mailService = new MailService("pengaduankabgor@gmail.com",user.getEmail(),"Registrasi anda di tolak #notify",text,text);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mailService.sendAuthenticated();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();


                FirebaseDatabase instance = FirebaseDatabase.getInstance();
                user.setKtpVerify(false);
                user.setKtpCancel(true);
                instance.getReference(Pref.REF_USERS).child(user.getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Pengguna berhasil di di tolak", Toast.LENGTH_SHORT).show();
                    }
                });

               observableNotif(user)
                       .subscribeOn(Schedulers.io())
                       .observeOn(AndroidSchedulers.mainThread())
                       .subscribe(new Consumer<String>() {
                           @Override
                           public void accept(String s) throws Exception {
                               System.out.println(s);
                           }
                       });

            }
        });


        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    private Observable<String> observableNotif(final User user){
        return io.reactivex.Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                String res = FcmUtils.sendMessageToDevice(user.getTokenFcm(),Pref.PREF_KTP,"2");
                e.onNext(res);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}