package app.ezza.laporpengaduan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.ezza.laporpengaduan.R;
import app.ezza.laporpengaduan.model.ActiveChat;
import app.ezza.laporpengaduan.utils.CircleTransform;

public class ActiveChatAdapter extends RecyclerView.Adapter<ActiveChatAdapter.ViewHolder> {

    private List<ActiveChat> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgUser;
        private final TextView textName;
        private final TextView textLastMessage;
        private final TextView textDate;
        private final TextView textNewMessage;

        View v;

        ViewHolder(View v) {

            super(v);
            this.v = v;

            imgUser = (ImageView) v.findViewById(R.id.img_user);
            textName = (TextView) v.findViewById(R.id.text_name);
            textLastMessage = (TextView) v.findViewById(R.id.text_last_message);
            textDate = (TextView) v.findViewById(R.id.text_date);
            textNewMessage = (TextView) v.findViewById(R.id.text_new_message);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ActiveChatAdapter(Context context, List<ActiveChat> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_active_chat, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.textNewMessage.setVisibility(View.VISIBLE);
        //holder.tv_title.setText(list.get(position).getNama_tour());
        ActiveChat activeChat = list.get(position);
        holder.textName.setText(activeChat.getName());
        holder.textLastMessage.setText(activeChat.getLastMessage());
        holder.textDate.setText(activeChat.getDate());
        holder.textNewMessage.setText(String.valueOf(activeChat.getNewMessage()));

        Picasso.with(context)
                .load(activeChat.getUser().getImageUrl())
                .placeholder(R.drawable.kabgorontalo)
                .transform(new CircleTransform())
                .into(holder.imgUser);

        if(activeChat.getNewMessage() == 0)
            holder.textNewMessage.setVisibility(View.GONE);


        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}