package app.ezza.laporpengaduan;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import app.ezza.laporpengaduan.model.User;

/**
 * Created by azisa on 6/26/2017.
 */

public class Pref {

    public static final String NOTIFICATION_LAPOR_FEEDBACK = "laporfeedback";

    public static final String PREF_KTP = "ktp";
    public static final String PREF_NAME = "app.pref";
    public static final String REF_USERS = "users";
    public static final String REF_INFO = "info";
    public static final String REF_CHAT = "chat";
    public static final String TOKEN_FIELD = "tokenFcm";
    public static final String REF_LAPOR = "lapor";
    private SharedPreferences preferences;
    private static final String KEY_LOGIN = "key.login";
    private static final String KEY_USER = "key_user";
    private static final String KEY_TOKEN = "key.token";

    private Gson gson;

    public Pref(SharedPreferences preferences) {
        this.preferences = preferences;
        this.gson = new Gson();
    }

    public void setLogin(boolean islogin){
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(KEY_LOGIN, islogin);
        edit.apply();
    }

    public boolean isLoginIn(){
        boolean iLogin = preferences.getBoolean(KEY_LOGIN, false);
        return iLogin;
    }

    public void storeUser(User user){
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(KEY_USER, gson.toJson(user));
        edit.apply();
    }

    public User getUser(){
        String json = preferences.getString(KEY_USER, null);
        User users = gson.fromJson(json, User.class);
        return users;
    }

    public void storeToken(String token){
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(KEY_TOKEN, token);
        edit.apply();
    }

    public String getToken(){
        String token = preferences.getString(KEY_TOKEN, null);
        return token;
    }

    public void clear(){
        SharedPreferences.Editor edit = preferences.edit();
        edit.clear();
        edit.apply();
    }

}
