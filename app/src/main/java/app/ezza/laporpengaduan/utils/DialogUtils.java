package app.ezza.laporpengaduan.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by azisa on 6/28/2017.
 */

public class DialogUtils {

    private static final DialogUtils ourInstance = new DialogUtils();

    public static DialogUtils getInstance() {
        return ourInstance;
    }

    private DialogUtils() {
    }

    public void showInfoDialog(Context context, String title, String msg, final InfoListener onInfoListener){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        onInfoListener.onDone(dialogInterface,i);
                    }
                }).create().show();

    }

    public void showYesNoDialog(Context context, String title, String msg, final YesNoListener yesNoListener){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        yesNoListener.onYes(dialogInterface,i);
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        yesNoListener.onNo(dialogInterface, i);
                    }
                })
                .create().show();

    }

    public void showErrorRetryDialog(Context context, String msg, final ErrorRetryListener errorRetryListener){
        if(context != null)
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ulangi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        errorRetryListener.onRetry(dialogInterface,i);
                    }
                })
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        errorRetryListener.onDone(dialogInterface,i);
                    }
                }).create().show();
    }

    public interface ErrorRetryListener {
        void onDone(DialogInterface dialogInterface, int position);
        void onRetry(DialogInterface dialogInterface, int position);
    }

    public interface InfoListener {
        void onDone(DialogInterface dialogInterface, int position);
    }

    public interface YesNoListener{
        void onYes(DialogInterface dialogInterface, int position);
        void onNo(DialogInterface dialogInterface, int position);
    }

}
