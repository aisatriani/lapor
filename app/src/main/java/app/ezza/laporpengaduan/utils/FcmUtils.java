package app.ezza.laporpengaduan.utils;

import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.ezza.laporpengaduan.Pref;
import app.ezza.laporpengaduan.model.Topik;
import app.ezza.laporpengaduan.model.User;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by azisa on 7/3/2017.
 */

public class FcmUtils {

    private static final String TAG = FcmUtils.class.getSimpleName();

    public static void registerTopik(Context context){
        Pref pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        User currentUser = pref.getUser();

        FirebaseMessaging.getInstance().subscribeToTopic(Topik.ALL);
        FirebaseMessaging.getInstance().subscribeToTopic(currentUser.getUid());
        switch (currentUser.getType()){
            case User.TYPE_BUPATI:
                Log.d(TAG, "registerTopik: bupati");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.BUPATI);
                break;
            case User.TYPE_KESEHATAN:
                Log.d(TAG, "registerTopik: kesehatan");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.KESEHATAN);
                break;
            case User.TYPE_MEMBER:
                Log.d(TAG, "registerTopik: member");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.MEMBER);
                break;
            case User.TYPE_PEMUKIMAN:
                Log.d(TAG, "registerTopik: pemukiman");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.PEMUKIMAN);
                break;
            case User.TYPE_PENDIDIKAN:
                Log.d(TAG, "registerTopik: pendidikan");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.PENDIDIKAN);
                break;
            case User.TYPE_POLSEK:
                Log.d(TAG, "registerTopik: polsek");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.POLSEK);
                break;
            case User.TYPE_PU:
                Log.d(TAG, "registerTopik: pu");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.PU);
                break;
            case User.TYPE_SOSIAL:
                Log.d(TAG, "registerTopik: sosial");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.SOSIAL);
                break;
            case User.TYPE_ADMINISTRATOR:
                Log.d(TAG, "registerTopik: administrator");
                FirebaseMessaging.getInstance().subscribeToTopic(Topik.ADMIN);
                break;
        }

    }

    public static void unregisterTopik(Context context){
        Pref pref = new Pref(context.getSharedPreferences(Pref.PREF_NAME, Context.MODE_PRIVATE));
        User currentUser = pref.getUser();

        FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.ALL);
        FirebaseMessaging.getInstance().unsubscribeFromTopic(currentUser.getUid());
        switch (currentUser.getType()){
            case User.TYPE_BUPATI:
                Log.d(TAG, "unregisterTopik: bupati");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.BUPATI);
                break;
            case User.TYPE_KESEHATAN:
                Log.d(TAG, "unregisterTopik: kesehatan");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.KESEHATAN);
                break;
            case User.TYPE_MEMBER:
                Log.d(TAG, "unregisterTopik: member");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.MEMBER);
                break;
            case User.TYPE_PEMUKIMAN:
                Log.d(TAG, "unregisterTopik: pemukiman");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.PEMUKIMAN);
                break;
            case User.TYPE_PENDIDIKAN:
                Log.d(TAG, "unregisterTopik: pendidikan");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.PENDIDIKAN);
                break;
            case User.TYPE_POLSEK:
                Log.d(TAG, "unregisterTopik: polsek");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.POLSEK);
                break;
            case User.TYPE_PU:
                Log.d(TAG, "unregisterTopik: PU");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.PU);
                break;
            case User.TYPE_SOSIAL:
                Log.d(TAG, "unregisterTopik: sosial");
                FirebaseMessaging.getInstance().unsubscribeFromTopic(Topik.SOSIAL);
                break;
        }
    }

    public static String postToFCM(String bodyString) throws IOException {

        OkHttpClient mClient = new OkHttpClient();

       final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + "AAAAS4evLKk:APA91bFh6l4HST_DdCizJ0mz6Zz2bW31sCSxFG1BmD7yRoPsSh48-l0luc-VNRKPJdF1F59RJGYrLFiVbeuwIvgz2u1qw1hiWiTHyizeHR1eR00kyLT51IZpniNTcIDXwIv3afQvnocg")
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

    public static String sendMessageToTopik(String topik, String data_key, String data_json) throws JSONException, IOException {
        JSONObject root = new JSONObject();
        root.put("to","/topics/"+topik);
        JSONObject dataMessage = new JSONObject();
        dataMessage.put(data_key,data_json);
        root.put("data",dataMessage);
        String response = postToFCM(root.toString());

        return response;
    }

    public static String sendMessageToDevice(String deviceToken, String data_key, String data_json) throws JSONException, IOException {
        JSONObject root = new JSONObject();
        root.put("to",deviceToken);
        JSONObject dataMessage = new JSONObject();
        dataMessage.put(data_key,data_json);
        root.put("data",dataMessage);
        String response = postToFCM(root.toString());

        return response;
    }
}
